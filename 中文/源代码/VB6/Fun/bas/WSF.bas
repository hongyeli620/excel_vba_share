Attribute VB_Name = "WSF"
'<component name="WSF">
'  <summary>自定义工作表函数模块</summary>
'  <createtime>2011-03-29 10:59</createtime>
'  <author email="hongyeli620@hotmail.com">Li Hongye</author>
'  <copyright>hongyeli620@hotmail.com 2009</copyright>
'  <system></system>
'</component>
Option Explicit

'<procedure name="JOIN">
'  <summary>模拟 VB 的 Join 函数</summary>
'  <returns datatype="String" />
'  <params>
'    <param name="target" datatype="Range">目标单元格区域</param>
'    <param name="delimeter" datatype="String">连接符</param>
'    <param name="skipblank" datatype="String">跳过空值</param>
'    <param name="transpose" datatype="String">转置单元格区域后再 JOIN</param>
'  </params>
'</procedure>
Function JOINA( _
    ByVal Target As Variant, _
    Optional ByVal delimeter As String = ",", _
    Optional ByVal skipblank As Boolean = False, _
    Optional ByVal Transpose As Boolean = False) As String
Attribute JOINA.VB_Description = "模拟 VB 的 JOIN 函数"
Attribute JOINA.VB_ProcData.VB_Invoke_Func = " \n17"

Dim vValues As Variant
Dim lLoopRow As Long
Dim lLoopColumn As Long
Dim sResult As String
    
' 错误处理
Dim lErrNumber As Long
Dim sMessage As String
Dim sSource As String

On Error GoTo Catch_Label
    
    'Debug.Print "JOIN", Application.ThisCell.Formula
    
    If TypeName(Target) = "Range" Then
        vValues = Target.value
    Else
        vValues = Target
    End If
    
    
    If F.GetArrayDimensionSize(vValues) = 1 Then
        vValues = F.ToTwoDimension(vValues)
    End If
    
    ' 转置数组后计算
    If Transpose And IsArray(vValues) Then
        vValues = F.Transpose(vValues)
    End If
    
    ' 单元格区域
    If IsArray(vValues) Then
        For lLoopRow = LBound(vValues, 1) To UBound(vValues, 1)
            For lLoopColumn = LBound(vValues, 2) To UBound(vValues, 2)
                If Not IsError(vValues(lLoopRow, lLoopColumn)) Then
                    If Not skipblank Or vValues(lLoopRow, lLoopColumn) & "" <> "" Then
                        sResult = sResult & vValues(lLoopRow, lLoopColumn) & delimeter
                    End If
                End If
            Next lLoopColumn
        Next lLoopRow
    
    ' 单元格
    Else
    End If
    
    ' 单元格区域
    If sResult <> "" Then
        sResult = Left(sResult, Len(sResult) - Len(delimeter))
        
    ' 单元格
    Else
        If Not IsArray(vValues) Then
            sResult = vValues
        End If
    End If
    
    JOINA = sResult

Finally_Label:
    Exit Function
    
Catch_Label:
    lErrNumber = Err.Number
    sSource = "Function JOIN of Module basWSF" & " | " & Err.Source
    sMessage = Err.Description

    Err.Raise lErrNumber, sSource, sMessage

End Function

'<procedure name="SPLITA">
'  <summary>模拟 VB 的 Split 函数</summary>
'  <returns datatype="String" />
'  <params>
'    <param name="target" datatype="String">要分割的字符串</param>
'    <param name="delimeter" datatype="String">连接符</param>
'    <param name="transpose" datatype="String">将 SPLIT 后的结果转置</param>
'  </params>
'</procedure>
Function SPLITA( _
    ByVal Target As Variant, _
    Optional ByVal delimeter As String = ",", _
    Optional ByVal Transpose As Boolean = False)
Attribute SPLITA.VB_Description = "模拟 VB 的 Split 函数"

Dim vCache As Variant
Dim vCache2() As Variant
Dim lRecordCount As Long

Dim vValues As Variant
Dim lElementCount As Long
Dim lLoop As Long
Dim vResult As Variant

' 错误处理
Dim lErrNumber As Long
Dim sMessage As String
Dim sSource As String

On Error GoTo Catch_Label
    
    'Debug.Print "SPLITA", Application.ThisCell.Formula
    
    If TypeName(Target) = "Range" Then
        vCache = Target.value
    Else
        vCache = Target
    End If
    
    lRecordCount = 1
    
    If IsArray(vCache) Then
        vCache2 = vCache
        If F.GetArrayDimensionSize(vCache2) = 1 Then
            vCache = F.ToTwoDimension(vCache2)
        End If
        
        If UBound(vCache, 1) > 1 Then
            lRecordCount = UBound(vCache, 1)
        Else
            lRecordCount = UBound(vCache, 2)
        End If
    End If
'
'        ' 仅处理一行或一列(列方向优先：多行多列按单列处理)
'        If target.Columns.Count > 1 Then
'            vCache = target.Columns(1).Value
'        Else
'            vCache = target.Rows(1).Value
'        End If
    
    If IsArray(vCache) Then
        For lLoop = LBound(vCache, 1) To UBound(vCache, 1)
            vValues = VBA.Split(vCache(lLoop, LBound(vCache)), delimeter, , vbTextCompare)
            
            If lElementCount < UBound(vCache) + 1 Then
                lElementCount = UBound(vCache) + 1
            End If
        Next lLoop
    Else
        vValues = VBA.Split(vCache, delimeter, , vbTextCompare)
        lElementCount = UBound(vValues) + 1
    End If
    
    If Transpose Then
        ReDim vResult(1 To lRecordCount, 1 To lElementCount)
    Else
        ReDim vResult(1 To lElementCount, 1 To lRecordCount)
    End If
    
    For lLoop = 1 To lElementCount
        If Transpose Then
            vResult(1, lLoop) = vValues(lLoop - 1)
        Else
            vResult(lLoop, 1) = vValues(lLoop - 1)
        End If
    Next lLoop
    
    SPLITA = vResult

Finally_Label:
    Exit Function
    
Catch_Label:
    lErrNumber = Err.Number
    sSource = "Function SPLITA of Module basWSF" & " | " & Err.Source
    sMessage = Err.Description

    Err.Raise lErrNumber, sSource, sMessage

End Function

'<procedure name="mTranslateExpression">
'  <summary>将含有变量的表达式转换成仅包含数值运算的表达式</summary>
'  <returns datatype="String" />
'  <params>
'    <param name="expression" datatype="String">表达式字符串</param>
'    <param name="variables" datatype="Variant">表达式变量的数组</param>
'  </params>
'</procedure>
Private Function mTranslateExpression( _
    ByVal Expression As String, _
    ByVal variables As Variant) As String

Dim vItem As Variant
Dim sReturn As String
Dim lLoopRow As Long

    sReturn = Expression
    
    For lLoopRow = 1 To UBound(variables, 1)
        sReturn = Replace(sReturn, variables(lLoopRow, 1), variables(lLoopRow, 2), , , vbTextCompare)
    Next lLoopRow
    
    mTranslateExpression = sReturn
    
End Function


Public Function DetectedMAC() As String
    Dim objWMIService, colItems, objItem
    ' 网卡MAC地址
    Set objWMIService = GetObject("winmgmts:\\.\root\cimv2")
    Set colItems = objWMIService.ExecQuery("SELECT MACAddress FROM Win32_NetworkAdapter WHERE ((MACAddress Is Not NULL) AND (Manufacturer <> 'Microsoft'))")
    For Each objItem In colItems
        DetectedMAC = objItem.MACAddress
        Exit For
    Next
    Set colItems = Nothing
End Function


' 判断文件是否已被其它应用打开
Public Function HasFileOpened(ByVal sPath As String) As Boolean
Attribute HasFileOpened.VB_Description = "判断文件是否已被其它应用打开"

Dim iFileNumber As Integer
    
On Error GoTo ErrHandler
    
    iFileNumber = FreeFile()
    
    '以独占方式打开 如果捕获到错误就是被占用了
    Open sPath For Binary Lock Read Write As iFileNumber
    Close iFileNumber
    
ErrHandler:
    If Err.Number = 70 Then
        HasFileOpened = True
    End If

End Function


Public Function TRANSPOSEA(Target)
Attribute TRANSPOSEA.VB_Description = "区域转置为数组"

    Dim sourceArray As Variant, targetArray As Variant
    Dim upperBound1 As Long, upperBound2 As Long
    Dim i As Long, j As Long
        
    sourceArray = Target.value
    upperBound1 = UBound(sourceArray, 1)
    upperBound2 = UBound(sourceArray, 2)
    
    ReDim targetArray(1 To upperBound2, 1 To upperBound1)
    
    For i = 1 To upperBound1
        For j = 1 To upperBound2
            targetArray(j, i) = sourceArray(i, j)
        Next j
    Next i
    
    TRANSPOSEA = targetArray
    
End Function


Public Function StringFormat( _
    ByVal sExpression As String, _
    ParamArray vReplace() As Variant)

Dim vElement As Variant
Dim lIndex As Long

Dim vItem As Variant
Dim vReturn As Variant
Dim bArray As Boolean

Dim lPos As Long

On Error GoTo Catch_Label
    
    lIndex = 0
    
    vReturn = sExpression
    For Each vElement In vReplace
        If Not IsObject(vElement) Then
            If IsArray(vElement) Then
                If Not bArray Then
                    ReDim vReturn(LBound(vElement) To UBound(vElement), 1 To 1)
                    lPos = LBound(vElement)
                End If
                For Each vItem In vElement
                    vReturn(lPos, 1) = Replace(vReturn, "{" & lIndex & "}", vItem)
                    lPos = lPos + 1
                Next vItem
                
                bArray = True
            Else
                vReturn = Replace(vReturn, "{" & lIndex & "}", vElement)
            End If
        Else
            If TypeName(vElement) = "Range" Then
                vReturn = Replace(vReturn, "{" & lIndex & "}", vElement.value)
            End If
        End If
        
        If bArray Then
            lPos = LBound(vElement)
        End If
        lIndex = lIndex + 1
    Next vElement
    
    StringFormat = vReturn

Finally_Label:
    Exit Function

Catch_Label:
    'Call G.Log.WriteError(VBA.Err, True)
    VBA.Err.Raise VBA.Err.Number, VBA.Err.Source, VBA.Err.Description

End Function


Public Function StringFormat2( _
    ByVal sExpression As String, _
    vReplaceArray As Variant)

Dim vElement As Variant
Dim lIndex As Long

Dim vItem As Variant
Dim vReturn As Variant
Dim bArray As Boolean

Dim lPos As Long

On Error GoTo Catch_Label
    
    lIndex = 0
    vReturn = sExpression
    
    For Each vElement In vReplaceArray
        If Not IsObject(vElement) Then
            If IsArray(vElement) Then
                If Not bArray Then
                    ReDim vReturn(LBound(vElement) To UBound(vElement), 1 To 1)
                    lPos = LBound(vElement)
                End If
                For Each vItem In vElement
                    vReturn(lPos, 1) = Replace(vReturn, "{" & lIndex & "}", vItem)
                    lPos = lPos + 1
                Next vItem
                
                bArray = True
            Else
                vReturn = Replace(vReturn, "{" & lIndex & "}", vElement)
            End If
        Else
            If TypeName(vElement) = "Range" Then
                vReturn = Replace(vReturn, "{" & lIndex & "}", vElement.value)
            End If
        End If
        
        If bArray Then
            lPos = LBound(vElement)
        End If
        lIndex = lIndex + 1
    Next vElement
    
    StringFormat2 = vReturn

Finally_Label:
    Exit Function

Catch_Label:
    'Call G.Log.WriteError(VBA.Err, True)
    VBA.Err.Raise VBA.Err.Number, VBA.Err.Source, VBA.Err.Description

End Function


