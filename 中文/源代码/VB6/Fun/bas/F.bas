Attribute VB_Name = "F"
' <summary>全局函数</summary>

Option Explicit

Public Function GetArrayDimensionSize(vArray) As Long
    Dim lCount As Long
    Dim lUBound As Long
    Dim i As Long
    
    On Error Resume Next
    
    For i = 1 To 1000
        lUBound = UBound(vArray, i)
        
        If Err.Number <> 0 Then
            Err.Clear
            GetArrayDimensionSize = lCount
            Exit Function
        End If
        
        lCount = lCount + 1
    Next i
    
End Function

'<procedure name="ToTwoDimension">
'  <summary>将一维数组转换为二位数组</summary>
'  <returns datatype="Variant" />
'  <params>
'    <param name="target" datatype="Variant" out="1" isarray="1">目标数组变量</param>
'  </params>
'</procedure>
Public Function ToTwoDimension( _
    ByRef Target As Variant, _
    Optional ByVal lArrayBase = 1) As Variant

Dim vCache() As Variant
Dim vReturn() As Variant
Dim lArrayLBound As Long
Dim lLoop As Long
    
    vCache = Target
    
    If F.GetArrayDimensionSize(vCache) = 1 Then
        lArrayLBound = LBound(vCache(), 1)
        
        ReDim vReturn(lArrayLBound To UBound(vCache, 1), lArrayBase To lArrayBase)
        
        For lLoop = lArrayLBound To UBound(vCache, 1)
            If IsObject(vCache(lLoop)) Then
                Set vReturn(lLoop, lArrayBase) = vCache(lLoop)
            Else
                vReturn(lLoop, lArrayBase) = vCache(lLoop)
            End If
        Next lLoop
    Else
        vReturn = vCache
    End If
    
    ToTwoDimension = vReturn

End Function

Public Function Transpose(vArray, Optional ByVal arrayBase = 1)
    Dim r As Variant
    Dim i As Long
    Dim j As Long
    Dim lU1 As Long
    Dim lL1 As Long
    Dim lU2 As Long
    Dim lL2 As Long
    
    arrayBase = IIf(arrayBase = 0, 0, 1)
    
    If F.GetArrayDimensionSize(vArray) = 1 Then
        vArray = F.ToTwoDimension(vArray, arrayBase)
        lU1 = arrayBase
        lL1 = arrayBase
    Else
        lU1 = UBound(vArray, 2)
        lL1 = LBound(vArray, 2)
    End If
    
    lU2 = UBound(vArray)
    lL2 = LBound(vArray)
    ReDim r(lL1 To lU1, lL2 To lU2)
    
    For i = LBound(vArray) To UBound(vArray)
        For j = LBound(vArray, 2) To UBound(vArray, 2)
            r(j, i) = vArray(i, j)
        Next j
    Next i
    
    Transpose = r
End Function

