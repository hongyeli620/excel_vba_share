VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ArrayFunc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_varArray As Variant
Private lngArrayDimSize As Long

Public Function V(vArray) As ArrayFunc
    If Not IsArray(vArray) Then
        VBA.Err.Raise 5000, "FuncLink.ArrayFunc.GetArray", "参数非数组。"
    End If
    lngArrayDimSize = F.GetArrayDimensionSize(vArray)
    If lngArrayDimSize > 2 Then
        VBA.Err.Raise 5000, "FuncLink.ArrayFunc.GetArray", "此版本最大支持二维数组。"
    End If
    m_varArray = vArray
    Set V = Me
End Function

Public Function Count() As IntFunc

    Dim value As Long
    If F.GetArrayDimensionSize(m_varArray) = 1 Then
        value = UBound(m_varArray) - LBound(m_varArray) + 1
    Else
        value = (UBound(m_varArray) - LBound(m_varArray) + 1) * (UBound(m_varArray, 2) - LBound(m_varArray, 2) + 1)
    End If
    Dim o As New IntFunc
    Set Count = o.V(value)
    
End Function

Public Function Rows() As IntFunc

    Dim value As Long
    value = UBound(m_varArray) - LBound(m_varArray) + 1
    Dim o As New IntFunc
    Set Rows = o.V(value)
    
End Function

Public Function Cols() As IntFunc

    Dim value As Long
    If F.GetArrayDimensionSize(m_varArray) = 1 Then
        value = 1
    Else
        value = (UBound(m_varArray, 2) - LBound(m_varArray, 2) + 1)
    End If
    Dim o As New IntFunc
    Set Cols = o.V(value)
    
End Function

Public Function Trim() As ArrayFunc
    Func 1
    Set Trim = Me
End Function

Public Function ToUpper() As ArrayFunc
    Func 2
    Set ToUpper = Me
End Function

Public Function ToLower() As ArrayFunc
    Func 3
    Set ToLower = Me
End Function

Public Function Replace(sFind As String, sReplace As String) As ArrayFunc
    Func 4, sFind, sReplace
    Set Replace = Me
End Function

Public Function ToNarrow() As ArrayFunc
    Func 5
    Set ToNarrow = Me
End Function

Public Function Transpose() As ArrayFunc
    m_varArray = F.Transpose(m_varArray)
    Set Transpose = Me
End Function

' G.ArrayObj.GetArray(array(3,4,"","s")).Format("#,##0.00")
Public Function Format( _
    ByVal sFormat As String _
    , Optional ByVal lFirstDayofWeek As VbDayOfWeek = vbSunday _
    , Optional ByVal lFirstWeekOfYear As VbFirstWeekOfYear = vbFirstJan1) As ArrayFunc
    
    Func 6, sFormat, lFirstDayofWeek, lFirstWeekOfYear
    Set Format = Me
End Function

Public Function StringFormat(ParamArray vReplace() As Variant) As ArrayFunc
    Dim V As Variant
    V = vReplace
    Func2 7, V
    Set StringFormat = Me
End Function

Private Sub Func(lFuncType As Long, ParamArray vParams() As Variant)
    Dim i As Long, j As Long
    
    If lngArrayDimSize = 1 Then
        For i = LBound(m_varArray) To UBound(m_varArray)
            m_InvokeMethod m_varArray(i), lFuncType, vParams
        Next i
    Else
        For i = LBound(m_varArray) To UBound(m_varArray)
            For j = LBound(m_varArray, 2) To UBound(m_varArray, 2)
                m_InvokeMethod m_varArray(i, j), lFuncType, vParams
            Next j
        Next i
    End If
End Sub

Private Sub Func2(lFuncType As Long, vParams As Variant)
    Dim i As Long, j As Long
    
    If lngArrayDimSize = 1 Then
        For i = LBound(m_varArray) To UBound(m_varArray)
            m_InvokeMethod m_varArray(i), lFuncType, vParams
        Next i
    Else
        For i = LBound(m_varArray) To UBound(m_varArray)
            For j = LBound(m_varArray, 2) To UBound(m_varArray, 2)
                m_InvokeMethod m_varArray(i, j), lFuncType, vParams
            Next j
        Next i
    End If
End Sub

' <summary>返回数组的某一列</summary>
Public Function Column(ByVal lColumn As Long) As ArrayFunc
    
Dim avReturn As Variant
Dim i As Long
Dim j As Long
Dim o As New ArrayFunc

On Error GoTo Catch_Label
    
    If F.GetArrayDimensionSize(m_varArray) < 2 Then
        m_varArray = F.ToTwoDimension(m_varArray)
    End If
    
    If lColumn < LBound(m_varArray, 2) Or lColumn > UBound(m_varArray, 2) Then
        VBA.Err.Raise 5000, "FuncLink.ArrayFunc.Column", "索引超出范围。"
    End If

    For i = LBound(m_varArray, 2) To UBound(m_varArray, 2)
        If i = lColumn Then
            ReDim avReturn(LBound(m_varArray) To UBound(m_varArray), 1 To 1)
            For j = LBound(m_varArray) To UBound(m_varArray)
                avReturn(j, 1) = m_varArray(j, i)
            Next j
            Exit For
        End If
    Next i
    
    Set Column = o.V(avReturn)
    
Finally_Label:
    Exit Function
    
Catch_Label:
'    Call Err.Push("NGL.Globals.Column")
'    Call Log.WriteError(Err, True)
'    GoTo Finally_Label
    VBA.Err.Raise VBA.Err.Number, VBA.Err.Source, VBA.Err.Description

End Function

Public Function FilterSimple( _
    ByVal vColIndex As Variant _
    , ByVal sValue1 As Variant _
    , Optional ByVal sOperator1 As String = "=" _
    , Optional ByVal sValue2 As Variant _
    , Optional ByVal sOperator2 As String = "=" _
    , Optional ByVal bMatchCase As Boolean _
    , Optional ByVal bIncludeHeader As Boolean = True) As ArrayFunc
    
    If F.GetArrayDimensionSize(m_varArray) <> 2 Then
        VBA.Err.Raise 5000, "FuncLink.ArrayFunc.FilterSimple", "仅支持对二维数组的筛选。"
    End If
    
    Dim o As New DataArrayHelper
    Call o.Init(m_varArray, bIncludeHeader)
    m_varArray = o.FilterSimple(vColIndex, sValue1, sOperator1, sValue2, sOperator2, bMatchCase)
    Set FilterSimple = Me
End Function

' <summary>返回按组织的条件进行数据筛选</summary>
Public Function Filter( _
    oFilters As Filters _
    , Optional ByRef vSourceRowArray As Variant _
    , Optional ByVal bIncludeHeader As Boolean = True) As ArrayFunc
    
    Dim o As New DataArrayHelper
    Call o.Init(m_varArray, bIncludeHeader)
    m_varArray = o.Filter(oFilters, vSourceRowArray)
    Set Filter = Me
End Function

' <summary>简单分类汇总</summary>
Public Function GroupBySimple( _
    ByVal vCategoryColIndex As Variant _
    , ByVal vSumColIndex1 As Variant _
    , Optional ByVal vSumColIndex2 As Variant _
    , Optional ByVal vSumColIndex3 As Variant _
    , Optional ByVal lSumType1 As SumTypeConstants = lvSTSum _
    , Optional ByVal lSumType2 As SumTypeConstants = lvSTSum _
    , Optional ByVal lSumType3 As SumTypeConstants = lvSTSum _
    , Optional ByVal bIncludeHeader As Boolean = True) As ArrayFunc

    Dim o As New DataArrayHelper
    Call o.Init(m_varArray, bIncludeHeader)
    m_varArray = o.GroupBySimple(vCategoryColIndex, vSumColIndex1, vSumColIndex2, vSumColIndex3, lSumType1, lSumType2, lSumType3)
    Set GroupBySimple = Me
End Function

' <summary>分类汇总</summary>
Public Function GroupBy( _
    vCategoryCol() As Variant _
    , vSumCol() As Variant _
    , vSumType() As SumTypeConstants _
    , Optional ByVal bIncludeHeader As Boolean = True) As ArrayFunc
    
    Dim o As New DataArrayHelper
    Call o.Init(m_varArray, bIncludeHeader)
    m_varArray = o.GroupBy(vCategoryCol, vSumCol, vSumType)
    Set GroupBy = Me
End Function

Public Function Resize(ByVal lHeight As Long _
    , ByVal lWidth As Long _
    , Optional ByVal bForce As Boolean = False _
    , Optional ByVal bSource As Boolean _
    , Optional ByVal bIncludeHeader As Boolean = True) As ArrayFunc

    Dim o As New DataArrayHelper
    Call o.Init(m_varArray, bIncludeHeader)
    m_varArray = o.Resize(lHeight, lWidth, bForce, bSource)
    Set Resize = Me
End Function

Public Function Expand(ByVal lRows As Long _
    , Optional ByVal lCols As Long _
    , Optional ByVal bForce As Boolean = False _
    , Optional ByVal bSource As Boolean _
    , Optional ByVal bIncludeHeader As Boolean = True) As ArrayFunc

    Dim o As New DataArrayHelper
    Call o.Init(m_varArray, bIncludeHeader)
    m_varArray = o.Expand(lRows, lCols, bForce, bSource)
    Set Expand = Me
End Function

' <summary>返回数组的某一行</summary>
Public Function Row(ByVal lRow As Long) As ArrayFunc
    
Dim avReturn As Variant
Dim i As Long
Dim j As Long

Dim o As New ArrayFunc

On Error GoTo Catch_Label
    
    If lRow < LBound(m_varArray) Or lRow > UBound(m_varArray) Then
        VBA.Err.Raise 5000, "FuncLink.ArrayFunc.Column", "索引超出范围。"
    End If

    If F.GetArrayDimensionSize(m_varArray) > 1 Then
        For i = LBound(m_varArray) To UBound(m_varArray)
            If i = lRow Then
                ReDim avReturn(1 To 1, LBound(m_varArray, 2) To UBound(m_varArray, 2))
                For j = LBound(m_varArray, 2) To UBound(m_varArray, 2)
                    avReturn(1, j) = m_varArray(i, j)
                Next j
                Exit For
            End If
        Next i
    End If
    
    Set Row = o.V(avReturn)
    
Finally_Label:
    Exit Function
    
Catch_Label:
'    Call Err.Push("NGL.Globals.Row")
'    Call Log.WriteError(Err, True)
'    GoTo Finally_Label
    VBA.Err.Raise VBA.Err.Number, VBA.Err.Source, VBA.Err.Description

End Function

Private Sub m_InvokeMethod( _
    ByRef vValue As Variant _
    , ByVal lFuncType As Long _
    , ByVal vParams As Variant)
            
            If Not IsObject(vValue) Then
                Select Case lFuncType
                    Case 1
                        vValue = Trim(vValue)
                    Case 2
                        vValue = UCase$(vValue)
                    Case 3
                        vValue = LCase$(vValue)
                    Case 4
                        vValue = VBA.Replace(vValue, vParams(0), vParams(1))
                    Case 5
                        vValue = StrConv(vValue, vbNarrow)
                    Case 6
                        vValue = VBA.Format(vValue, vParams(0), vParams(1), vParams(2))
                    Case 7
                        vValue = WSF.StringFormat2(vValue, vParams)
                End Select
            End If
End Sub

Public Property Get value()
Attribute value.VB_UserMemId = 0
    value = m_varArray
End Property

Public Function Join( _
    Optional ByVal sDelimiter As String = "," _
    , Optional ByVal bIgnoreBlank As Boolean = False _
    , Optional ByVal bTranspose As Boolean = False) As StringFunc
        
    Dim r As String
    Dim o As New StringFunc
    
    r = WSF.JOINA(m_varArray, sDelimiter, bIgnoreBlank, bTranspose)
    Set Join = o.V(r)
    
End Function
