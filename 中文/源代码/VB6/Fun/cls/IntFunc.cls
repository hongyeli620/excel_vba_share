VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IntFunc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_lngValue As Long

Public Function V(value As Long) As IntFunc
    m_lngValue = value
    Set V = Me
End Function

' G.IntObj.V(123456789).Format("#,##0.00")
Public Function Format( _
    ByVal sFormat As String _
    , Optional ByVal lFirstDayofWeek As VbDayOfWeek = vbSunday _
    , Optional ByVal lFirstWeekOfYear As VbFirstWeekOfYear = vbFirstJan1) As StringFunc
    
    Dim value As String
    Dim o As New StringFunc
    value = VBA.Format(m_lngValue, sFormat, lFirstDayofWeek, lFirstWeekOfYear)
    Set Format = o.V(value)
End Function

Public Property Get value() As Long
Attribute value.VB_UserMemId = 0
    value = m_lngValue
End Property

