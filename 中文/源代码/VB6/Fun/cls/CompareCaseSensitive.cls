VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CompareCaseSensitive"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Option Compare Binary

Implements ICompare

Public Function ICompare_Contants(String1, String2) As Boolean
    ICompare_Contants = (String1 Like String2)
End Function

Public Function ICompare_DoesNotContant(String1, String2) As Boolean
    ICompare_DoesNotContant = Not (String1 Like String2)
End Function

Public Function ICompare_BeginWith(String1, String2) As Boolean
    ICompare_BeginWith = (String1 Like String2 & "*")
End Function

Public Function ICompare_EndWith(String1, String2) As Boolean
    ICompare_EndWith = (String1 Like "*" & String2)
End Function

Private Function ICompare_Equals(String1 As Variant, String2 As Variant) As Boolean
    ICompare_Equals = (StrComp(String1, String2, vbBinaryCompare) = 0)
End Function

Private Function ICompare_DoesNotEqual(String1 As Variant, String2 As Variant) As Boolean
    ICompare_DoesNotEqual = Not (StrComp(String1, String2, vbBinaryCompare) = 0)
End Function


