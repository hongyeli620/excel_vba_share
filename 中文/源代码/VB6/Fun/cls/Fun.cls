VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Fun"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private m_objFun As FunHelper


Private Sub Class_Terminate()
    Call Dispose
End Sub

Public Sub Dispose()
    On Error Resume Next
    Set m_objFun = Nothing
End Sub


Public Property Get Fun() As FunHelper
    
    If m_objFun Is Nothing Then
        Set m_objFun = New FunHelper
    End If
    Set Fun = m_objFun

End Property

