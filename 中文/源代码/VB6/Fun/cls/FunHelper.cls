VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "FunHelper"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_objArrayFunc As ArrayFunc
Private m_objStringFunc As StringFunc
Private m_objIntFunc As IntFunc

Private Sub Class_Terminate()
    Call Dispose
End Sub

Public Sub Dispose()
    On Error Resume Next
    Set m_objArrayFunc = Nothing
    Set m_objStringFunc = Nothing
    Set m_objIntFunc = Nothing
End Sub

Public Property Get ArrayFunc(value) As ArrayFunc
    
    If m_objArrayFunc Is Nothing Then
        Set m_objArrayFunc = New ArrayFunc
    End If
    Set ArrayFunc = m_objArrayFunc.V(value)

End Property


Public Property Get StringFunc(ByVal value As String) As StringFunc

    If m_objStringFunc Is Nothing Then
        Set m_objStringFunc = New StringFunc
    End If
    Set StringFunc = m_objStringFunc.V(value)

End Property


Public Property Get IntFunc(ByVal value As Long) As IntFunc

    If m_objIntFunc Is Nothing Then
        Set m_objIntFunc = New IntFunc
    End If
    Set IntFunc = m_objIntFunc.V(value)

End Property


