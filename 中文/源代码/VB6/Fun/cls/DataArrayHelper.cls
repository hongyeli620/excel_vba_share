VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DataArrayHelper"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Enum SumTypeConstants
    lvSTSum
    lvSTCount
    lvSTAverage
    lvSTMax
    lvSTMin
End Enum

Private m_blnDirty As Boolean
Private m_varDataArray As Variant
Private m_varDataSourceArray As Variant
Private m_varDataHeaderArray As Variant
Private m_blnIncludeHeader As Boolean
Private m_objCompare As ICompare
Private m_objCompareFactory As New CompareFactory

'<procedure name="IncludeHeader" rw="_/r_">
'  <summary>获取数据区域是否包含标题行</summary>
'  <returns datatype="Boolean">
'    <return value="True">包含</return>
'    <return value="False">不包含</return>
'  </returns>
'</procedure>
Public Property Get IncludeHeader() As Boolean
Attribute IncludeHeader.VB_Description = "获取数据区域是否包含标题行"
    IncludeHeader = m_blnIncludeHeader
End Property

'<procedure name="DataArray" rw="__/r_">
'  <summary>获取上一次过滤后的数据的二维数组</summary>
'  <returns datatype="Variant" />
'</procedure>
Public Property Get DataArray() As Variant
Attribute DataArray.VB_Description = "获取上一次过滤后的数据的二维数组"
    DataArray = m_varDataArray
End Property

'<procedure name="DataSourceArray" rw="__/r_">
'  <summary>获取源数据区域的二维数组</summary>
'  <returns datatype="Variant" />
'</procedure>
Public Property Get DataSourceArray() As Variant
Attribute DataSourceArray.VB_Description = "获取源数据区域的二维数组"
    DataSourceArray = m_varDataSourceArray
End Property

'<procedure name="DataSourceArray" rw="__/r_">
'  <summary>获取源数据标题区域的二维数组</summary>
'  <returns datatype="Variant" />
'</procedure>
Public Property Get DataHeaderArray() As Variant
    DataHeaderArray = m_varDataHeaderArray
End Property

'<procedure name="Dirty" rw="__/rw">
'  <summary>获取源数据区域是否被滤过过</summary>
'  <returns datatype="Boolean">
'    <return value="True">是</return>
'    <return value="False">否</return>
'  </returns>
'  <remarks>如果返回 true,表示已经过滤并缓存了数据,可以通过 Data 属性获取上一次过滤的结果</remarks>
'</procedure>
Public Property Get Dirty() As Boolean
Attribute Dirty.VB_Description = "获取源数据区域是否被滤过过"
    Dirty = m_blnDirty
End Property

Public Property Let Dirty(ByVal blnDirty As Boolean)
    m_blnDirty = blnDirty
End Property

'<procedure name="Init">
'  <summary>初始化</summary>
'  <params>
'    <param name="Target" datatype="Excel.Range" out="1">目标区域</param>
'    <param name="IncludeHeader" datatype="Boolean" optional="1" default="True" >True=目标区域包含标题行</param>
'  </params>
'</procedure>
Public Sub Init( _
    Target As Variant, _
    Optional ByVal IncludeHeader As Boolean = True)
    
Dim avHeader As Variant
Dim i As Long
    
Dim lErrNumber As Long
Dim sMessage As String
Dim sSource As String

On Error GoTo Catch_Label

    m_blnIncludeHeader = IncludeHeader
    m_varDataHeaderArray = avHeader
    
    If IsObject(Target) Then
        If TypeName(Target) = "Range" Then
            If Target.Count = 1 Then
                ReDim m_varDataSourceArray(1 To 1, 1 To 1)
                m_varDataSourceArray(1, 1) = Target.value
            Else
                m_varDataSourceArray = Target.value
            End If
        Else
            VBA.Err.Raise vbObjectError + 100, "", "无效的对象参数类型 Target。"
        End If
    Else
        If IsArray(Target) Then
            m_varDataSourceArray = Target
        Else
            VBA.Err.Raise vbObjectError + 100, "", "无效的参数类型 Target。"
        End If
    End If
    
    If m_blnIncludeHeader Then
        ReDim avHeader(1 To 1, LBound(m_varDataSourceArray, 2) To UBound(m_varDataSourceArray, 2))
        For i = LBound(avHeader, 2) To UBound(avHeader, 2)
            avHeader(1, i) = m_varDataSourceArray(LBound(m_varDataSourceArray), i)
        Next i
        m_varDataHeaderArray = avHeader
    End If

Finally_Label:
    Exit Sub

Catch_Label:
    lErrNumber = VBA.Err.Number
    sSource = "Sub Init of Class Module DataArrayHelper" & " | " & VBA.Err.Source
    sMessage = VBA.Err.Description

    VBA.Err.Raise lErrNumber, sSource, sMessage
End Sub

'<procedure name="FilterSimple">
'  <summary>单列过滤并返回符合条件的数据的二维数组</summary>
'  <returns datatype="Variant" />
'  <params>
'    <param name="ColIndex" datatype="Variant" >列索引。如果 IncludeHeader = True, 则可以用标题名作为列索威；否则只能将数组区域的列标顺序号作为列索引值传入。</param>
'    <param name="Value1" datatype="Variant" >查找的值。</param>
'    <param name="Operator1" datatype="String" optional="1" default="=" >操作符。用于标识条件匹配类型，支持 =, >, <, >=, <=, <>, like, not like</param>
'    <param name="Value2" datatype="Variant" >查找的值。用于区间条件的另一个条件值</param>
'    <param name="Operator2" datatype="String" optional="1" default="=" >操作符。用于标识条件匹配类型，支持 =, >, <, >=, <=, <>, like, not like</param>
'    <param name="MatchCase" datatype="Boolean" optional="1" default="False" >匹配大小写</param>
'  </params>
'</procedure>
Public Function FilterSimple( _
    ByVal ColIndex As Variant, _
    ByVal Value1 As Variant, _
    Optional ByVal Operator1 As String = "=", _
    Optional ByVal Value2 As Variant, _
    Optional ByVal Operator2 As String = "=", _
    Optional ByVal MatchCase As Boolean) As Variant
Attribute FilterSimple.VB_Description = "单列过滤并返回符合条件的数据的二维数组"

Dim lCol As Long
Dim lStartRow As Long
Dim i As Long, j As Long
Dim avRecords As Variant
Dim n As Long

Dim bMatch As Boolean

Dim lErrNumber As Long
Dim sMessage As String
Dim sSource As String

On Error GoTo Catch_Label
    
    ' 重新初始化用于存储过滤数据的缓存
    m_varDataArray = avRecords
    If F.GetArrayDimensionSize(m_varDataArray) = 1 Then
        m_varDataArray = F.ToTwoDimension(m_varDataArray)
    End If
    
    Me.Dirty = True
    
    If IsNumeric(ColIndex) Then
        lCol = ColIndex
    Else
        If Me.IncludeHeader Then
            '如果传入的是列标题名称，需要计算列位置
            lCol = GetColIndex(ColIndex)
        End If
    End If
    
    ' 源数据无数据行
    If (UBound(m_varDataSourceArray) = 1 And Me.IncludeHeader) _
        Or (UBound(m_varDataSourceArray) = 0 And Not Me.IncludeHeader) Then
        
        GoTo Finally_Label
    End If
    
    ' 起始行
    lStartRow = -Me.IncludeHeader + 1
    
    ReDim avRecords(LBound(m_varDataSourceArray, 1) To UBound(m_varDataSourceArray, 1), LBound(m_varDataSourceArray, 2) To UBound(m_varDataSourceArray, 2))
    
    If MatchCase Then
        Set m_objCompare = m_objCompareFactory.GetInstance(vbBinaryCompare)
    Else
        Set m_objCompare = m_objCompareFactory.GetInstance(vbTextCompare)
    End If
    
    For i = lStartRow To UBound(m_varDataSourceArray)
        If IsNull(m_varDataSourceArray(i, lCol)) Then
            m_varDataSourceArray(i, lCol) = ""
        End If
        
        Select Case LCase(Operator1)
            Case "="
                If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value1) And TypeName(Value1) <> "String" Then
                    m_varDataSourceArray(i, lCol) = 0
                End If
                bMatch = m_objCompare.Equals(Value1, m_varDataSourceArray(i, lCol))
            Case ">"
                If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value1) And TypeName(Value1) <> "String" Then
                    m_varDataSourceArray(i, lCol) = 0
                End If
                bMatch = (m_varDataSourceArray(i, lCol) > Value1)
                
            Case "<"
                If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value1) And TypeName(Value1) <> "String" Then
                    m_varDataSourceArray(i, lCol) = 0
                End If
                bMatch = (m_varDataSourceArray(i, lCol) < Value1)
                
            Case ">="
                If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value1) And TypeName(Value1) <> "String" Then
                    m_varDataSourceArray(i, lCol) = 0
                End If
                bMatch = (m_varDataSourceArray(i, lCol) >= Value1)
                
            Case "<="
                If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value1) And TypeName(Value1) <> "String" Then
                    m_varDataSourceArray(i, lCol) = 0
                End If
                bMatch = (m_varDataSourceArray(i, lCol) <= Value1)
                
            Case "<>"
                If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value1) And TypeName(Value1) <> "String" Then
                    m_varDataSourceArray(i, lCol) = 0
                End If
                bMatch = m_objCompare.DoesNotEqual(Value1, m_varDataSourceArray(i, lCol))
                
            Case "like"
                bMatch = m_objCompare.Contants(m_varDataSourceArray(i, lCol), Value1)
                
            Case "not like"
                bMatch = m_objCompare.DoesNotContant(m_varDataSourceArray(i, lCol), Value1)
                
        End Select
        
        If Not IsEmpty(Value2) And Not IsMissing(Value2) Then
            Select Case LCase(Operator2)
                Case "="
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value2) And TypeName(Value2) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    bMatch = bMatch And (m_objCompare.Equals(Value2, m_varDataSourceArray(i, lCol)))
                Case ">"
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value2) And TypeName(Value2) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    bMatch = bMatch And (m_varDataSourceArray(i, lCol) > Value2)
                    
                Case "<"
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value2) And TypeName(Value2) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    bMatch = bMatch And (m_varDataSourceArray(i, lCol) < Value2)
                    
                Case ">="
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value2) And TypeName(Value2) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    bMatch = bMatch And (m_varDataSourceArray(i, lCol) >= Value2)
                    
                Case "<="
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value2) And TypeName(Value2) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    bMatch = bMatch And (m_varDataSourceArray(i, lCol) <= Value2)
                    
                Case "<>"
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value2) And TypeName(Value2) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    bMatch = bMatch And (m_objCompare.DoesNotEqual(Value2, m_varDataSourceArray(i, lCol)))
                    
                Case "like"
                    bMatch = bMatch And (m_objCompare.Contants(m_varDataSourceArray(i, lCol), Value2))
                    
                Case "not like"
                    bMatch = bMatch And (m_objCompare.DoesNotContant(m_varDataSourceArray(i, lCol), Value2))
                    
            End Select
        End If
        
        If bMatch Then
            n = n + 1
            
            For j = LBound(m_varDataSourceArray, 2) To UBound(m_varDataSourceArray, 2)
                avRecords(n, j) = m_varDataSourceArray(i, j)
            Next j
        End If
    Next i
    
    If n > 0 Then
        ReDim m_varDataArray(LBound(avRecords, 1) To n, LBound(avRecords, 2) To UBound(avRecords, 2))
        
        For i = LBound(m_varDataArray, 1) To n
            For j = LBound(m_varDataArray, 2) To UBound(m_varDataArray, 2)
                m_varDataArray(i, j) = avRecords(i, j)
            Next j
        Next i
    End If
    
    FilterSimple = m_varDataArray
    
Finally_Label:
    Exit Function

Catch_Label:
    lErrNumber = Err.Number
    sSource = "Function FilterSimple of Class Module DataArrayHelper" & " | " & Err.Source
    sMessage = Err.Description

    Err.Raise lErrNumber, sSource, sMessage
End Function

'<procedure name="Filter">
'  <summary>按给定条件过滤源数据并返回二维数组</summary>
'  <returns datatype="Variant" />
'  <params>
'    <param name="Filters" datatype="Filters" out="1">过滤条件集合对象</param>
'  </params>
'</procedure>
Public Function Filter( _
    Filters As Filters _
    , Optional ByRef SourceRowArray As Variant) As Variant

Dim lCol As Long
Dim lStartRow As Long
Dim i As Long, j As Long
Dim avRecords As Variant
Dim n As Long

Dim bMatch As Boolean
Dim alRowArray() As Long

Dim oFilter As Filter
Dim vValue As Variant
Dim lCounter As Long

Dim lErrNumber As Long
Dim sMessage As String
Dim sSource As String

On Error GoTo Catch_Label
    
    ' 重新初始化用于存储过滤数据的缓存
    m_varDataArray = avRecords
    If F.GetArrayDimensionSize(m_varDataArray) = 1 Then
        m_varDataArray = F.ToTwoDimension(m_varDataArray)
    End If
    Me.Dirty = True
    
    ' 源数据无数据行
    If (UBound(m_varDataSourceArray) = 1 And Me.IncludeHeader) _
        Or (UBound(m_varDataSourceArray) = 0 And Not Me.IncludeHeader) Then
        
        GoTo Finally_Label
    End If
    
    ' 起始行
    lStartRow = -Me.IncludeHeader + 1
    
    ReDim avRecords(LBound(m_varDataSourceArray, 1) To UBound(m_varDataSourceArray, 1) _
        , LBound(m_varDataSourceArray, 2) To UBound(m_varDataSourceArray, 2))
    
    For i = lStartRow To UBound(m_varDataSourceArray)
        j = 0
        'bMatch = True
        
        For Each oFilter In Filters
            If oFilter.MatchCase Then
                Set m_objCompare = m_objCompareFactory.GetInstance(vbBinaryCompare)
            Else
                Set m_objCompare = m_objCompareFactory.GetInstance(vbTextCompare)
            End If
            
            If IsNumeric(oFilter.ColumnIndex) Then
                lCol = oFilter.ColumnIndex
            Else
                If Me.IncludeHeader Then
                    '如果传入的是列标题名称，需要计算列位置
                    lCol = GetColIndex(oFilter.ColumnIndex)
                End If
            End If
            
            If IsNull(m_varDataSourceArray(i, lCol)) Then
                m_varDataSourceArray(i, lCol) = ""
            End If
            
            vValue = oFilter.value
            
            Select Case oFilter.Operator
                Case DFHOperatorConstants.dfhOperatorEquals
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(oFilter.value) And TypeName(oFilter.value) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    
                    If lCounter = 0 Then
                        bMatch = (m_objCompare.Equals(vValue, m_varDataSourceArray(i, lCol)))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (m_objCompare.Equals(vValue, m_varDataSourceArray(i, lCol)))
                        Else
                            bMatch = bMatch Or (m_objCompare.Equals(vValue, m_varDataSourceArray(i, lCol)))
                        End If
                    End If
                Case DFHOperatorConstants.dfhOperatorDoesNotEqual
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(oFilter.value) And TypeName(oFilter.value) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    
                    If lCounter = 0 Then
                        bMatch = (m_objCompare.DoesNotEqual(vValue, m_varDataSourceArray(i, lCol)))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (m_objCompare.DoesNotEqual(vValue, m_varDataSourceArray(i, lCol)))
                        Else
                            bMatch = bMatch Or (m_objCompare.DoesNotEqual(vValue, m_varDataSourceArray(i, lCol)))
                        End If
                    End If
                Case DFHOperatorConstants.dfhOperatorGreaterThan
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(oFilter.value) And TypeName(oFilter.value) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    
                    If lCounter = 0 Then
                        bMatch = (vValue > m_varDataSourceArray(i, lCol))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (vValue > m_varDataSourceArray(i, lCol))
                        Else
                            bMatch = bMatch Or (vValue > m_varDataSourceArray(i, lCol))
                        End If
                    End If
                Case DFHOperatorConstants.dfhOperatorEqualsAndGreaterThan
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(oFilter.value) And TypeName(oFilter.value) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    
                    If lCounter = 0 Then
                        bMatch = (vValue >= m_varDataSourceArray(i, lCol))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (vValue >= m_varDataSourceArray(i, lCol))
                        Else
                            bMatch = bMatch Or (vValue >= m_varDataSourceArray(i, lCol))
                        End If
                    End If
                Case DFHOperatorConstants.dfhOperatorLessThan
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(oFilter.value) And TypeName(oFilter.value) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    
                    If lCounter = 0 Then
                        bMatch = (vValue < m_varDataSourceArray(i, lCol))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (vValue < m_varDataSourceArray(i, lCol))
                        Else
                            bMatch = bMatch Or (vValue < m_varDataSourceArray(i, lCol))
                        End If
                    End If
                Case DFHOperatorConstants.dfhOperatorEqualsAndLessThan
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(oFilter.value) And TypeName(oFilter.value) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    
                    If lCounter = 0 Then
                        bMatch = (vValue <= m_varDataSourceArray(i, lCol))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (vValue <= m_varDataSourceArray(i, lCol))
                        Else
                            bMatch = bMatch Or (vValue <= m_varDataSourceArray(i, lCol))
                        End If
                    End If
                Case DFHOperatorConstants.dfhOperatorContants
                    If lCounter = 0 Then
                        bMatch = (m_objCompare.Contants(m_varDataSourceArray(i, lCol), vValue))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (m_objCompare.Contants(m_varDataSourceArray(i, lCol), vValue))
                        Else
                            bMatch = bMatch Or (m_objCompare.Contants(m_varDataSourceArray(i, lCol), vValue))
                        End If
                    End If
                Case DFHOperatorConstants.dfhOperatorDoesNotContant
                    If lCounter = 0 Then
                        bMatch = (m_objCompare.DoesNotContant(m_varDataSourceArray(i, lCol), vValue))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (m_objCompare.DoesNotContant(m_varDataSourceArray(i, lCol), vValue))
                        Else
                            bMatch = bMatch Or (m_objCompare.DoesNotContant(m_varDataSourceArray(i, lCol), vValue))
                        End If
                    End If
                Case DFHOperatorConstants.dfhOperatorBeginWith
                    If lCounter = 0 Then
                        bMatch = (m_objCompare.BeginWith(m_varDataSourceArray(i, lCol), vValue))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (m_objCompare.BeginWith(m_varDataSourceArray(i, lCol), vValue))
                        Else
                            bMatch = bMatch Or (m_objCompare.BeginWith(m_varDataSourceArray(i, lCol), vValue))
                        End If
                    End If
                Case DFHOperatorConstants.dfhOperatorEndWith
                    If lCounter = 0 Then
                        bMatch = (m_objCompare.EndWith(m_varDataSourceArray(i, lCol), vValue))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (m_objCompare.EndWith(m_varDataSourceArray(i, lCol), vValue))
                        Else
                            bMatch = bMatch Or (m_objCompare.EndWith(m_varDataSourceArray(i, lCol), vValue))
                        End If
                    End If
            End Select
            
            lCounter = lCounter + 1
        Next oFilter
        
        lCounter = 0
        If bMatch Then
            ReDim Preserve alRowArray(n)
            alRowArray(n) = i
            
            n = n + 1
            
            For j = LBound(m_varDataSourceArray, 2) To UBound(m_varDataSourceArray, 2)
                avRecords(n, j) = m_varDataSourceArray(i, j)
            Next j
        End If
    Next i
    
    If n > 0 Then
        ReDim m_varDataArray(LBound(avRecords, 1) To n, LBound(avRecords, 2) To UBound(avRecords, 2))
        
        For i = LBound(m_varDataArray, 1) To n
            For j = LBound(m_varDataArray, 2) To UBound(m_varDataArray, 2)
                m_varDataArray(i, j) = avRecords(i, j)
            Next j
        Next i
    End If
    
    SourceRowArray = alRowArray
    Filter = m_varDataArray
    
Finally_Label:
    Exit Function

Catch_Label:
    lErrNumber = VBA.Err.Number
    sSource = "Function Filter of Class Module DataArrayHelper" & " | " & VBA.Err.Source
    sMessage = VBA.Err.Description

    VBA.Err.Raise lErrNumber, sSource, sMessage
End Function

'<summary>重新设定数组大小</summary>
'<param name="bForce">对于一维数组如果 lWidth 大于 1， 则强制转换为二维数组；否则，对于一维数组忽略 lWidth 参数</param>
'<param name="bSource">基于[true=数据源;false=结果]的数组设定大小</param>
Public Function Resize(ByVal lHeight As Long _
    , ByVal lWidth As Long _
    , Optional ByVal bForce As Boolean = False _
    , Optional ByVal bSource As Boolean) As Variant

Dim i As Long, j As Long, i2 As Long, j2 As Long
Dim V As Variant
Dim vTarget As Variant

Dim lErrNumber As Long
Dim sMessage As String
Dim sSource As String

On Error GoTo Catch_Label
    
    vTarget = IIf(bSource, m_varDataSourceArray, m_varDataArray)
    
    If F.GetArrayDimensionSize(vTarget) = 1 Then
        If bForce And lWidth > 0 Then
            ReDim V(LBound(vTarget) To lHeight + LBound(vTarget) - 1, LBound(vTarget, 2) To LBound(vTarget, 2) + lWidth - 1)
            
            i2 = IIf(lHeight < UBound(vTarget) - LBound(vTarget) + 1, UBound(V), UBound(vTarget))
            For i = LBound(vTarget) To i2
                V(i, 1) = vTarget(i, 1)
            Next i
        Else
            V = vTarget
            ReDim Preserve V(LBound(vTarget) To LBound(vTarget) + lHeight - 1)
        End If
    Else
        ReDim V(LBound(vTarget) To lHeight + LBound(vTarget) - 1, LBound(vTarget, 2) To LBound(vTarget, 2) + lWidth - 1)
        
        i2 = IIf(lHeight < UBound(vTarget) - LBound(vTarget) + 1, UBound(V), UBound(vTarget))
        j2 = IIf(lWidth < UBound(vTarget, 2) - UBound(vTarget, 2) + 1, UBound(V, 2), UBound(vTarget, 2))
        
        For i = LBound(vTarget) To i2
            For j = LBound(vTarget, 2) To j2
                V(i, j) = vTarget(i, j)
            Next j
        Next i
    End If
    
    Resize = V
    
Finally_Label:
    Exit Function

Catch_Label:
    lErrNumber = VBA.Err.Number
    sSource = "FuncLink.DataArrayHelper.Resize" & " | " & VBA.Err.Source
    sMessage = VBA.Err.Description

    VBA.Err.Raise lErrNumber, sSource, sMessage

End Function

'<summary>在原数组基础上额外扩展指定的行数或列数</summary>
'<param name="lRows">在原数组基础上额外扩展 lRows 行数</param>
'<param name="lCols">在原数组基础上额外扩展 lCols 列数</param>
'<param name="bForce">对于一维数组如果 lCols 大于 0， 则强制转换为二维数组；否则，对于一维数组忽略 lCols 参数</param>
'<param name="bSource">基于[true=数据源;false=结果]的数组设定大小</param>
Public Function Expand(ByVal lRows As Long _
    , Optional ByVal lCols As Long _
    , Optional ByVal bForce As Boolean = False _
    , Optional ByVal bSource As Boolean) As Variant

Dim i As Long, j As Long, i2 As Long, j2 As Long
Dim V As Variant
Dim vTarget As Variant

Dim lErrNumber As Long
Dim sMessage As String
Dim sSource As String

On Error GoTo Catch_Label
    
    vTarget = IIf(bSource, m_varDataSourceArray, m_varDataArray)
    
    If F.GetArrayDimensionSize(vTarget) = 1 Then
        If bForce And lCols > 0 Then
            ReDim V(LBound(vTarget) To UBound(vTarget) - LBound(vTarget) + 1 + lRows, 1 To lCols + 1)
            
            i2 = IIf(lRows < 0, lRows + LBound(vTarget) - 1, UBound(vTarget))
            For i = LBound(vTarget) To i2
                V(i, 1) = vTarget(i, 1)
            Next i
        Else
            V = vTarget
            ReDim Preserve V(LBound(vTarget) To LBound(vTarget) + lRows - 1)
        End If
    Else
        ReDim V(LBound(vTarget) To UBound(vTarget) - LBound(vTarget) + 1 + lRows _
            , LBound(vTarget, 2) To UBound(vTarget, 2) - LBound(vTarget, 2) + 1 + lCols)
        
        i2 = IIf(lRows < 0, lRows + LBound(vTarget) - 1, UBound(vTarget))
        j2 = IIf(lCols < 0, lCols + LBound(vTarget, 2) - 1, UBound(vTarget, 2))
        
        For i = LBound(vTarget) To i2
            For j = LBound(vTarget, 2) To j2
                V(i, j) = vTarget(i, j)
            Next j
        Next i
    End If
    
    Expand = V
    
Finally_Label:
    Exit Function

Catch_Label:
    lErrNumber = VBA.Err.Number
    sSource = "FuncLink.DataArrayHelper.Expand" & " | " & VBA.Err.Source
    sMessage = VBA.Err.Description

    VBA.Err.Raise lErrNumber, sSource, sMessage

End Function

Public Function GroupBySimple( _
    ByVal vCategoryColIndex As Variant _
    , ByVal vSumColIndex1 As Variant _
    , Optional ByVal vSumColIndex2 As Variant _
    , Optional ByVal vSumColIndex3 As Variant _
    , Optional ByVal lSumType1 As SumTypeConstants = lvSTSum _
    , Optional ByVal lSumType2 As SumTypeConstants = lvSTSum _
    , Optional ByVal lSumType3 As SumTypeConstants = lvSTSum) As Variant

Dim vCatCol(0) As Variant
Dim vSumCol() As Variant
Dim vSumType() As SumTypeConstants

Dim lErrNumber As Long
Dim sMessage As String
Dim sSource As String

On Error GoTo Catch_Label

    vCatCol(0) = vCategoryColIndex
    ReDim vSumCol(0)
    ReDim vSumType(0)
    vSumCol(0) = vSumColIndex1
    vSumType(0) = lSumType1
    
    If Not IsMissing(vSumColIndex2) Then
        If Not IsEmpty(vSumColIndex2) Then
            ReDim Preserve vSumCol(1)
            ReDim Preserve vSumType(1)
            vSumCol(1) = vSumColIndex2
            vSumType(1) = lSumType2
        End If
    End If
    
    If Not IsMissing(vSumColIndex3) Then
        If Not IsEmpty(vSumColIndex3) Then
            ReDim Preserve vSumCol(2)
            ReDim Preserve vSumType(2)
            vSumCol(2) = vSumColIndex3
            vSumType(2) = lSumType3
        End If
    End If
    
    GroupBySimple = GroupBy(vCatCol, vSumCol, vSumType)
    
Finally_Label:
    Exit Function

Catch_Label:
    lErrNumber = VBA.Err.Number
    sSource = "FuncLink.DataArrayHelper.GroupBySimple" & " | " & VBA.Err.Source
    sMessage = VBA.Err.Description

    VBA.Err.Raise lErrNumber, sSource, sMessage
    
End Function

Public Function GroupBy( _
    CategoryCol() As Variant _
    , SumCol() As Variant _
    , SumType() As SumTypeConstants) As Variant

Dim alCatColArray() As Long
Dim alSumColArray() As Long
Dim alPerCount() As Long '每个分组的行数 'GroupByStruc
Dim V As Variant
Dim lStartRow As Long
Dim lRows As Long
Dim i As Long, j As Long, k As Long

#If DEBUG_MODE = 1 Then
    Dim oKey As Scripting.Dictionary
#Else
    Dim oKey As Object
#End If

Dim lErrNumber As Long
Dim sMessage As String
Dim sSource As String

On Error GoTo Catch_Label
    
    ' 重新初始化用于存储过滤数据的缓存
    m_varDataArray = V
    If F.GetArrayDimensionSize(m_varDataArray) = 1 Then
        m_varDataArray = F.ToTwoDimension(m_varDataArray)
    End If
    Me.Dirty = True
    
    ' 起始行
    lStartRow = -Me.IncludeHeader + 1
    
    ReDim alCatColArray(1 To UBound(CategoryCol) - LBound(CategoryCol) + 1)
    For i = LBound(CategoryCol) To UBound(CategoryCol)
        If IsNumeric(CategoryCol(i)) Then
            alCatColArray(i + 1) = CategoryCol(i)
        Else
            If Me.IncludeHeader Then
                '如果传入的是列标题名称，需要计算列位置
                alCatColArray(i + 1) = GetColIndex(CategoryCol(i))
            Else
                VBA.Err.Raise vbObjectError + 100, "", "无效的分类索引。"
            End If
        End If
    Next i
    
    ReDim alSumColArray(1 To UBound(SumCol) - LBound(SumCol) + 1)
    For i = LBound(SumCol) To UBound(SumCol)
        If IsNumeric(SumCol(i)) Then
            alSumColArray(i + 1) = SumCol(i)
        Else
            If Me.IncludeHeader Then
                '如果传入的是列标题名称，需要计算列位置
                alSumColArray(i + 1) = GetColIndex(SumCol(i))
            Else
                VBA.Err.Raise vbObjectError + 100, "", "无效的分类索引。"
            End If
        End If
    Next i
    
    lRows = UBound(m_varDataSourceArray)
    
    ReDim V(LBound(m_varDataSourceArray) To lRows _
        , LBound(m_varDataSourceArray) To LBound(m_varDataSourceArray) + LBound(alCatColArray) + UBound(alSumColArray) - 1)
    
    If Me.IncludeHeader Then
        i = LBound(V)
        For j = LBound(alCatColArray) To UBound(alCatColArray)
            V(i, j) = m_varDataHeaderArray(1, alCatColArray(j))
        Next j
        k = j - 1
        For j = LBound(alSumColArray) To UBound(alSumColArray)
            V(i, j + k) = m_varDataHeaderArray(1, alSumColArray(j))
        Next j
    End If
    
    lRows = IIf(Me.IncludeHeader, 1, 0)
    
    ' 源数据无数据行
    If (UBound(m_varDataSourceArray) = 1 And Me.IncludeHeader) _
        Or (UBound(m_varDataSourceArray) = 0 And Not Me.IncludeHeader) Then
        
    Else
        Dim sKey As String
        Dim lRow As Long
        Set oKey = CreateObject("Scripting.Dictionary")
        
        For i = lStartRow To UBound(m_varDataSourceArray)
            sKey = ""
            For j = 1 To UBound(alCatColArray)
                sKey = sKey & m_varDataSourceArray(i, alCatColArray(j)) & "|"
            Next j
            
            If oKey.Exists(sKey) Then
                lRow = oKey(sKey)
                
                ' 记录各分类的记录明细行数，用于计算平均值
                alPerCount(lRow) = alPerCount(lRow) + 1
                
                ' 写入分类值
                For j = 1 To UBound(alCatColArray)
                    V(lRow, j) = m_varDataSourceArray(i, alCatColArray(j))
                Next j
                
                ' 写入聚合值
                k = j - 1
                For j = 1 To UBound(alSumColArray)
                    If SumType(j - 1) = lvSTSum Or SumType(j - 1) = lvSTAverage Then
                        V(lRow, j + k) = V(lRow, j + k) + m_varDataSourceArray(i, alSumColArray(j))
                    ElseIf SumType(j - 1) = lvSTCount Then
                        V(lRow, j + k) = V(lRow, j + k) + 1
                    ElseIf SumType(j - 1) = lvSTMax Then
                        If m_varDataSourceArray(i, alSumColArray(j)) > V(lRow, j + k) Then
                             V(lRow, j + k) = m_varDataSourceArray(i, alSumColArray(j))
                        End If
                    ElseIf SumType(j - 1) = lvSTMin Then
                        If m_varDataSourceArray(i, alSumColArray(j)) < V(lRow, j + k) Then
                             V(lRow, j + k) = m_varDataSourceArray(i, alSumColArray(j))
                        End If
                    End If
                Next j
            Else
                lRows = lRows + 1
                oKey.Add sKey, lRows
                
                ReDim Preserve alPerCount(1 To lRows)
                lRow = lRows
                
                ' 记录各分类的记录明细行数，用于计算平均值
                alPerCount(lRow) = 1
                
                ' 写入分类值
                For j = 1 To UBound(alCatColArray)
                    V(lRow, j) = m_varDataSourceArray(i, alCatColArray(j))
                Next j
                
                ' 写入聚合值
                k = j - 1
                For j = 1 To UBound(alSumColArray)
                    If SumType(j - 1) = lvSTSum Or SumType(j - 1) = lvSTAverage Or SumType(j - 1) = lvSTMax Or SumType(j - 1) = lvSTMin Then
                        V(lRow, j + k) = m_varDataSourceArray(i, alSumColArray(j))
                    ElseIf SumType(j - 1) = lvSTCount Then
                        V(lRow, j + k) = V(lRow, j + k) + 1
                    End If
                Next j
            
            End If
        Next i
        
        k = UBound(alCatColArray)
        For j = 1 To UBound(alSumColArray)
            If SumType(j - 1) = lvSTAverage Then
                For i = lStartRow To lRows
                    V(i, j + k) = V(i, j + k) / alPerCount(i)
                Next i
            End If
        Next j
    End If
    
    'lRows = lRows + IIf(Me.IncludeHeader, 1, 0)
    m_varDataArray = V
    m_varDataArray = Resize(lRows, UBound(V, 2) - LBound(V, 2) + 1, False)
    
    GroupBy = m_varDataArray
    
Finally_Label:
    Exit Function

Catch_Label:
    lErrNumber = VBA.Err.Number
    sSource = "FuncLink.DataArrayHelper.GroupBy" & " | " & VBA.Err.Source
    sMessage = VBA.Err.Description

    VBA.Err.Raise lErrNumber, sSource, sMessage
    
End Function

Private Function GetColIndex( _
    ByVal Fieldname As String) As Long

Dim lCol As Long
Dim lLoop As Long

    For lLoop = LBound(m_varDataSourceArray, 2) To UBound(m_varDataSourceArray, 2)
        If StrComp(m_varDataSourceArray(1, lLoop), Fieldname, vbTextCompare) = 0 Then
            lCol = lLoop
            Exit For
        End If
    Next lLoop
    
    GetColIndex = lCol
End Function

Public Sub Dispose()
    On Error Resume Next
    Set m_objCompare = Nothing
    If Not m_objCompareFactory Is Nothing Then
        m_objCompareFactory.Dispose
    End If
    Set m_objCompareFactory = Nothing
End Sub
