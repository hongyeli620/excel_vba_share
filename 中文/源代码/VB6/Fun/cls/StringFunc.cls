VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "StringFunc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_strValue As String

Public Function V(value As String) As StringFunc
    m_strValue = value
    Set V = Me
End Function

' G.StringObj.V("100000").Format("#,##0.00")
Public Function Format( _
    ByVal sFormat As String _
    , Optional ByVal lFirstDayofWeek As VbDayOfWeek = vbSunday _
    , Optional ByVal lFirstWeekOfYear As VbFirstWeekOfYear = vbFirstJan1) As StringFunc
    
    m_strValue = VBA.Format(m_strValue, sFormat, lFirstDayofWeek, lFirstWeekOfYear)
    Set Format = Me
End Function

' G.StringObj.V("abcdefgaabb").Replace("a","x")
Public Function Replace( _
    sFind As String _
    , sReplace As String _
    , Optional lStart As Long = 1 _
    , Optional lCount As Long = -1 _
    , Optional lCompare As VbCompareMethod = vbBinaryCompare) As StringFunc
    
    m_strValue = VBA.Replace(m_strValue, sFind, sReplace, lStart, lCount, lCompare)
    Set Replace = Me
End Function

'?G.StringObj.V("1234586789").InStr(1,"8")
Public Function InStr(Optional ByVal Start _
    , Optional ByVal String2 _
    , Optional ByVal Compare As VbCompareMethod = vbBinaryCompare) As IntFunc
    
    Dim value As Long
    value = VBA.InStr(Start, m_strValue, String2, Compare)
    Dim o As New IntFunc
    Set InStr = o.V(value)
End Function

'?G.StringObj.V("1234586789").InStrRev("8")
Function InStrRev(StringMatch As String _
    , Optional Start As Long = -1 _
    , Optional Compare As VbCompareMethod = vbBinaryCompare) As IntFunc
    
    Dim value As Long
    value = VBA.InStrRev(m_strValue, StringMatch, Start, Compare)
    Dim o As New IntFunc
    Set InStrRev = o.V(value)
End Function

Public Function Split( _
    Optional ByVal sDelimiter As String = ",", _
    Optional ByVal bTranspose As Boolean = False) As ArrayFunc
    
    Dim o As New ArrayFunc
    Set Split = o.V(WSF.SPLITA(m_strValue, sDelimiter, bTranspose))
    
End Function

Public Function ToChar() As ArrayFunc
    Dim sArray() As String
    Dim i As Long
    
    If Len(m_strValue) > 1 Then
        ReDim sArray(Len(m_strValue) - 1)
        For i = 0 To Len(m_strValue) - 1
            sArray(i) = Mid(m_strValue, i + 1, 1)
        Next i
    Else
        ReDim sArray(0)
        sArray(0) = m_strValue
    End If

    Dim o As New ArrayFunc
    Set ToChar = o.V(sArray)
End Function

Public Property Get value() As String
Attribute value.VB_UserMemId = 0
    value = m_strValue
End Property
