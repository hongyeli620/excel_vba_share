Attribute VB_Name = "WSF"
'<component name="WSF">
'  <summary>自定义工作表函数模块</summary>
'  <createtime>2011-03-29 10:59</createtime>
'  <author email="hongyeli620@hotmail.com">Li Hongye</author>
'  <copyright>hongyeli620@hotmail.com 2009</copyright>
'  <system></system>
'</component>
Option Explicit

'<procedure name="JOIN">
'  <summary>模拟 VB 的 Join 函数</summary>
'  <returns datatype="String" />
'  <params>
'    <param name="target" datatype="Range">目标单元格区域</param>
'    <param name="delimeter" datatype="String">连接符</param>
'    <param name="skipblank" datatype="String">跳过空值</param>
'    <param name="transpose" datatype="String">转置单元格区域后再 JOIN</param>
'  </params>
'</procedure>
Function JOIN( _
    ByVal target As Variant, _
    Optional ByVal delimeter As String = ",", _
    Optional ByVal skipblank As Boolean = False, _
    Optional ByVal transpose As Boolean = False) As String
Attribute JOIN.VB_Description = "模拟 VB 的 JOIN 函数"
Attribute JOIN.VB_ProcData.VB_Invoke_Func = " \n17"

Dim vValues As Variant
Dim lLoopRow As Long
Dim lLoopColumn As Long
Dim sResult As String
    
' 错误处理
Dim lErrNumber As Long
Dim sMessage As String
Dim sSource As String

On Error GoTo Catch_Label
    
    'Debug.Print "JOIN", Application.ThisCell.Formula
    
    If TypeOf target Is Range Then
        vValues = target.Value
    Else
        vValues = target
    End If
    
    
    If Globals.GetArrayDimensionSize(vValues) = 1 Then
        vValues = Globals.ToTwoDimension(vValues)
    End If
    
    ' 转置数组后计算
    If transpose And IsArray(vValues) Then
        vValues = Application.WorksheetFunction.transpose(vValues)
    End If
    
    ' 单元格区域
    If IsArray(vValues) Then
        For lLoopRow = LBound(vValues, 1) To UBound(vValues, 1)
            For lLoopColumn = LBound(vValues, 2) To UBound(vValues, 2)
                If Not IsError(vValues(lLoopRow, lLoopColumn)) Then
                    If Not skipblank Or vValues(lLoopRow, lLoopColumn) & "" <> "" Then
                        sResult = sResult & vValues(lLoopRow, lLoopColumn) & delimeter
                    End If
                End If
            Next lLoopColumn
        Next lLoopRow
    
    ' 单元格
    Else
    End If
    
    ' 单元格区域
    If sResult <> "" Then
        sResult = Left(sResult, Len(sResult) - Len(delimeter))
        
    ' 单元格
    Else
        If Not IsArray(vValues) Then
            sResult = vValues
        End If
    End If
    
    JOIN = sResult

Finally_Label:
    Exit Function
    
Catch_Label:
    lErrNumber = Err.Number
    sSource = "Function JOIN of Module basWSF" & " | " & Err.Source
    sMessage = Err.Description

    Err.Raise lErrNumber, sSource, sMessage

End Function

'<procedure name="SPLITA">
'  <summary>模拟 VB 的 Split 函数</summary>
'  <returns datatype="String" />
'  <params>
'    <param name="target" datatype="String">要分割的字符串</param>
'    <param name="delimeter" datatype="String">连接符</param>
'    <param name="transpose" datatype="String">将 SPLIT 后的结果转置</param>
'  </params>
'</procedure>
Function SPLITA( _
    ByVal target As Variant, _
    Optional ByVal delimeter As String = ",", _
    Optional ByVal transpose As Boolean = False)
Attribute SPLITA.VB_Description = "模拟 VB 的 Split 函数"

Dim vCache As Variant
Dim vCache2() As Variant
Dim lRecordCount As Long

Dim vValues As Variant
Dim lElementCount As Long
Dim lLoop As Long
Dim vResult As Variant

' 错误处理
Dim lErrNumber As Long
Dim sMessage As String
Dim sSource As String

On Error GoTo Catch_Label
    
    Debug.Print "SPLITA", Application.ThisCell.Formula
    
    If TypeOf target Is Range Then
        vCache = target.Value
    Else
        vCache = target
    End If
    
    lRecordCount = 1
    
    If IsArray(vCache) Then
        vCache2 = vCache
        If Globals.GetArrayDimensionLength(vCache2) = 1 Then
            vCache = Globals.ToTwoDimension(vCache2)
        End If
        
        If UBound(vCache, 1) > 1 Then
            lRecordCount = UBound(vCache, 1)
        Else
            lRecordCount = UBound(vCache, 2)
        End If
    End If
'
'        ' 仅处理一行或一列(列方向优先：多行多列按单列处理)
'        If target.Columns.Count > 1 Then
'            vCache = target.Columns(1).Value
'        Else
'            vCache = target.Rows(1).Value
'        End If
    
    If IsArray(vCache) Then
        For lLoop = LBound(vCache, 1) To UBound(vCache, 1)
            vValues = VBA.Split(vCache(lLoop, LBound(vCache)), delimeter, , vbTextCompare)
            
            If lElementCount < UBound(vCache) + 1 Then
                lElementCount = UBound(vCache) + 1
            End If
        Next lLoop
    Else
        vValues = VBA.Split(vCache, delimeter, , vbTextCompare)
        lElementCount = UBound(vValues) + 1
    End If
    
    If transpose Then
        ReDim vResult(1 To lRecordCount, 1 To lElementCount)
    Else
        ReDim vResult(1 To lElementCount, 1 To lRecordCount)
    End If
    
    For lLoop = 1 To lElementCount
        If transpose Then
            vResult(1, lLoop) = vValues(lLoop - 1)
        Else
            vResult(lLoop, 1) = vValues(lLoop - 1)
        End If
    Next lLoop
    
    SPLITA = vResult

Finally_Label:
    Exit Function
    
Catch_Label:
    lErrNumber = Err.Number
    sSource = "Function SPLITA of Module basWSF" & " | " & Err.Source
    sMessage = Err.Description

    Err.Raise lErrNumber, sSource, sMessage

End Function

'<procedure name="DISTINCTCHAR">
'  <summary>去除字符串中的重复字符</summary>
'  <returns datatype="Variant" />
'  <params>
'    <param name="target" datatype="Variant">单元格引用或字符串常量</param>
'  </params>
'</procedure>
Public Function DISTINCTCHAR(target)
Attribute DISTINCTCHAR.VB_Description = "去除字符串中的重复字符"
    
Dim vCache As Variant
Dim sReturn As String
Dim vReturn As Variant

Dim lPosition As Long

Dim lLoopRow As Long
Dim lLoopColumn As Long
    
    ' 缓存表达式
    If TypeOf target Is Excel.Range Then
        ' 参数为单元格区域
        vCache = target.Value
        
        If Not IsArray(vCache) Then
            sReturn = vCache
        End If
    Else
        ' 参数为数组常量
        If IsArray(target) Then
            vCache = target
            
            ' 将一维数据强制转换为二维数组（同一之后的算法）
            vCache = Globals.ToTwoDimension(vCache)
        Else
            sReturn = target
        End If
    End If
    
    If IsArray(vCache) Then
        ReDim vReturn(LBound(vCache, 1) To UBound(vCache, 1), LBound(vCache, 2) To UBound(vCache, 2))
        
        For lLoopRow = LBound(vCache, 1) To UBound(vCache, 1)
            For lLoopColumn = LBound(vCache, 2) To UBound(vCache, 2)
                sReturn = vCache(lLoopRow, lLoopColumn)
                
                lPosition = 1
                sReturn = Mid(sReturn, 1, 1) & Replace(sReturn, Mid(sReturn, 1, 1), "")
                
                Do While lPosition < Len(sReturn)
                    sReturn = Left(sReturn, lPosition) & Mid(sReturn, lPosition + 1, 1) & Replace(Mid(sReturn, lPosition + 1), Mid(sReturn, lPosition + 1, 1), "")
                    lPosition = lPosition + 1
                Loop
                
                vReturn(lLoopRow, lLoopColumn) = sReturn
            Next lLoopColumn
        Next lLoopRow
        
        DISTINCTCHAR = vReturn
    Else
        lPosition = 1
        sReturn = Mid(sReturn, 1, 1) & Replace(sReturn, Mid(sReturn, 1, 1), "")
        
        Do While lPosition < Len(sReturn)
            sReturn = Left(sReturn, lPosition) & Mid(sReturn, lPosition + 1, 1) & Replace(Mid(sReturn, lPosition + 1), Mid(sReturn, lPosition + 1, 1), "")
            lPosition = lPosition + 1
        Loop
        
        DISTINCTCHAR = sReturn
    End If
    
End Function

'<procedure name="EVALUATE2">
'  <summary>计算带变量的表达式</summary>
'  <returns datatype="Variant" />
'  <params>
'    <param name="target" datatype="Variant">表达式单元格区域或数组</param>
'    <param name="variables" datatype="Variant" optional="1">变量列表单元格区域或数组</param>
'  </params>
'  <remarks>
'    1) target 支持单元格区域和数组常量
'    2) variables 为至少包含两列单元格区域，或二维数组常量
'  </remarks>
'</procedure>
Public Function EVALUATE2( _
    ByVal target As Variant, _
    Optional ByVal variables As Variant) As Variant
Attribute EVALUATE2.VB_Description = "计算带变量的表达式"

Dim vExpressions As Variant     ' 缓存表达式
Dim vVariables As Variant       ' 缓存变量

Dim lLoopRow As Long
Dim sExpression As String

Dim vReturn As Variant
    
    ' 缓存变量(如：S00-1)
    If TypeOf variables Is Excel.Range Then
        vVariables = variables.Value
    Else
        vVariables = variables
    End If
    
    ' 缓存表达式
    If TypeOf target Is Excel.Range Then
        ' 参数为单元格区域
        vExpressions = target.Value
        
        If Not IsArray(vExpressions) Then
            sExpression = vExpressions
        End If
    Else
        ' 参数为数组常量
        If IsArray(target) Then
            vExpressions = target
            
            ' 将一维数据强制转换为二维数组（同一之后的算法）
            vExpressions = Globals.ToTwoDimension(vExpressions)
        Else
            sExpression = target
        End If
    End If
    
    ' 支持数组公式
    If IsArray(vExpressions) Then
        ReDim vReturn(1 To UBound(vExpressions, 1), 1 To 1)
        
        For lLoopRow = 1 To UBound(vExpressions, 1)
            If vExpressions(lLoopRow, 1) & "" <> "" Then
                If Not IsMissing(vVariables) Then
                    sExpression = mTranslateExpression(vExpressions(lLoopRow, 1), vVariables)
                Else
                    sExpression = vExpressions(lLoopRow, 1)
                End If
                vReturn(lLoopRow, 1) = Application.Evaluate(sExpression)
            Else
                vReturn(lLoopRow, 1) = ""
            End If
        Next lLoopRow
    
    ' 单值处理
    Else
        If Not IsMissing(vVariables) Then
            sExpression = mTranslateExpression(sExpression, vVariables)
        End If
        
        If sExpression <> "" Then
            vReturn = Application.Evaluate(sExpression)
        Else
            vReturn = ""
        End If
    End If
    
    EVALUATE2 = vReturn
    
End Function

'<procedure name="mTranslateExpression">
'  <summary>将含有变量的表达式转换成仅包含数值运算的表达式</summary>
'  <returns datatype="String" />
'  <params>
'    <param name="expression" datatype="String">表达式字符串</param>
'    <param name="variables" datatype="Variant">表达式变量的数组</param>
'  </params>
'</procedure>
Private Function mTranslateExpression( _
    ByVal expression As String, _
    ByVal variables As Variant) As String

Dim vItem As Variant
Dim sReturn As String
Dim lLoopRow As Long

    sReturn = expression
    
    For lLoopRow = 1 To UBound(variables, 1)
        sReturn = Replace(sReturn, variables(lLoopRow, 1), variables(lLoopRow, 2), , , vbTextCompare)
    Next lLoopRow
    
    mTranslateExpression = sReturn
    
End Function

Public Function GETCELLUNIQUE( _
    target As Variant, _
    Optional seperator As String = ",")

Dim sValue
Dim asValue() As String
Dim sReturn As String
Dim lFound As Long

    If TypeOf target Is Excel.Range Then
        sReturn = target.Cells(1).Value
    Else
        sReturn = target
    End If
    
    asValue = Split(sReturn, seperator)
    sReturn = seperator & sReturn & seperator
    
    For Each sValue In asValue
        lFound = InStr(1, sReturn, sValue)
        sReturn = Left(sReturn, lFound - 1) & sValue & seperator & Replace(Mid(sReturn, lFound + Len(sValue & seperator)), sValue & seperator, "")
    Next sValue
    
    GETCELLUNIQUE = Mid(sReturn, 2, Len(sReturn) - 2)
    
End Function

'<procedure name="RemoveAllSpaceChar>
'  <summary>去除指定区域单元格内的空格</summary>
'  <returns datatype="Variant" />
'  <params>
'    <param name="target" datatype="Excel.Range">目标区域</param>
'  </params>
'  <remarks>
'    1. 处理后的结果会被强制转换为文本
'    2. 返回与原区域大小相同的数组
'  </remarks>
'</procedure>
Function RemoveAllSpaceChar(target As Range) As Variant
Attribute RemoveAllSpaceChar.VB_Description = "去除指定区域单元格内的空格"

Dim avRecords As Variant
Dim lRow As Long, lCol As Long, lRows As Long, lCols As Long
    
    avRecords = target.Value
    
    If IsArray(avRecords) Then
        lRows = UBound(avRecords, 1)
        lCols = UBound(avRecords, 2)
        
        For lCol = 1 To lCols
            For lRow = 1 To lRows
                avRecords(lRow, lCol) = "'" & Replace(avRecords(lRow, lCol), " ", "")
            Next lRow
        Next lCol
    End If
    
    RemoveAllSpaceChar = avRecords
    
End Function

Public Function SYSTEM_GET_MAC() As String
    Dim objWMIService, colItems, objItem
    ' 网卡MAC地址
    Set objWMIService = GetObject("winmgmts:\\.\root\cimv2")
    Set colItems = objWMIService.ExecQuery("SELECT MACAddress FROM Win32_NetworkAdapter WHERE ((MACAddress Is Not NULL) AND (Manufacturer <> 'Microsoft'))")
    For Each objItem In colItems
        SYSTEM_GET_MAC = objItem.MACAddress
        Exit For
    Next
    Set colItems = Nothing
End Function

Function LToChin(ByVal Str As String) As Variant
Attribute LToChin.VB_Description = "汉字转拼音"
'汉字转拼音
Dim i  As Integer, strTmp As String, intlen As Integer
    strTmp = Str
    intlen = Len(Str)
    For i = 1 To intlen
        On Error Resume Next
        strTmp = StrConv(strTmp, vbNarrow)
        If Asc(Str) > 0 Or Err.Number = 1004 Then LToChin = ""
        LToChin = LToChin & WorksheetFunction.VLookup(strTmp, [{"吖","a";"八","b";"嚓","c";"咑","d";"鵽","e";"发","f";"猤","g";"铪","h";"夻","j";"咔","k";"垃","l";"嘸","m";"旀","n";"噢","o";"妑","p";"七","q";"囕","r";"仨","s";"他","t";"屲","w";"夕","x";"丫","y";"帀","z"}], 2)
        strTmp = Mid(strTmp, 2)
    Next i

End Function


'<procedure name="CurrentPage">
'  <summary>返回单元格所在页码</summary>
'  <returns datatype="Variant" />
'  <params>
'    <param name="vorder" datatype="Long" optional="1" default="-1" >页顺序。默认值为-1，跟页面设置选项走。</param>
'    <param name="firstPageNumber" datatype="Long" optional="1" default="1" >起始页码。默认 1</param>
'  </params>
'</procedure>
Public Function CurrentPage( _
    Optional ByVal vorder As Long = -1, _
    Optional ByVal firstPageNumber As Long = 1)
Attribute CurrentPage.VB_Description = "返回单元格所在页码"

Dim oSheet As Worksheet
Dim oThisCell As Range
Dim oHPageBreak As HPageBreak
Dim oVPageBreak As VPageBreak

Dim lPosH As Long
Dim lPosV As Long

Dim lVCount As Long
Dim lHCount As Long
    
' 错误处理
Dim lErrNumber As Long
Dim sMessage As String
Dim sSource As String

On Error GoTo Catch_Label

    Application.Volatile
    
    Set oThisCell = Application.ThisCell
    Set oSheet = Application.ThisCell.Parent
    
    lPosH = 1
    lPosV = 1
    lVCount = 0
    lHCount = 0
    
    For Each oHPageBreak In oSheet.HPageBreaks
        lHCount = lHCount + 1
        If oThisCell.Row >= oHPageBreak.Location.Row Then
            lPosH = lPosH + 1
        End If
    Next oHPageBreak
    
    For Each oVPageBreak In oSheet.VPageBreaks
        lVCount = lVCount + 1
        If oThisCell.Column >= oVPageBreak.Location.Column Then
            lPosV = lPosV + 1
        End If
    Next oVPageBreak
    
    If vorder = -1 Then
        vorder = oSheet.PageSetup.Order
    End If
    
    If vorder = 1 Then
        CurrentPage = (lPosV - 1) * (lHCount + 1) + lPosH + (firstPageNumber - 1)
    Else
        CurrentPage = (lPosH - 1) * (lVCount + 1) + lPosV + (firstPageNumber - 1)
    End If
    

Finally_Label:
    Exit Function
    
Catch_Label:
    lErrNumber = Err.Number
    sSource = "Function CurrentPage of Module WSF" & " | " & Err.Source
    sMessage = Err.Description

    Err.Raise lErrNumber, sSource, sMessage
    
End Function


' 判断文件是否已被其它应用打开
Public Function HasFileOpened(ByVal sPath As String) As Boolean
Attribute HasFileOpened.VB_Description = "判断文件是否已被其它应用打开"

Dim iFileNumber As Integer
    
On Error GoTo ErrHandler
    
    iFileNumber = FreeFile()
    
    '以独占方式打开 如果捕获到错误就是被占用了
    Open sPath For Binary Lock Read Write As iFileNumber
    Close iFileNumber
    
ErrHandler:
    If Err.Number = 70 Then
        HasFileOpened = True
    End If

End Function


Public Function TRANSPOSEA(target)
Attribute TRANSPOSEA.VB_Description = "区域转置为数组"

    Dim sourceArray As Variant, targetArray As Variant
    Dim upperBound1 As Long, upperBound2 As Long
    Dim i As Long, j As Long
        
    sourceArray = target.Value
    upperBound1 = UBound(sourceArray, 1)
    upperBound2 = UBound(sourceArray, 2)
    
    ReDim targetArray(1 To upperBound2, 1 To upperBound1)
    
    For i = 1 To upperBound1
        For j = 1 To upperBound2
            targetArray(j, i) = sourceArray(i, j)
        Next j
    Next i
    
    TRANSPOSEA = targetArray
    
End Function


Public Function StringFormat( _
    ByVal sExpression As String, _
    ParamArray vReplace() As Variant) As String
Attribute StringFormat.VB_Description = "字符串格式化"

Dim vElement As Variant
Dim lIndex As Long

On Error GoTo Catch_Label
    
    lIndex = 0
    
    For Each vElement In vReplace
        If Not IsObject(vElement) Then
            sExpression = Replace(sExpression, "{" & lIndex & "}", vElement)
        Else
            If TypeName(vElement) = "Range" Then
                sExpression = Replace(sExpression, "{" & lIndex & "}", vElement.Value)
            End If
        End If
        lIndex = lIndex + 1
    Next vElement
    
    StringFormat = sExpression

Finally_Label:
    Exit Function

Catch_Label:
    'Call Globals.Log.WriteError(VBA.Err, True)
    VBA.Err.Raise VBA.Err.Number, VBA.Err.Source, VBA.Err.Description

End Function


Public Function LastDay(ByVal sDate As String)

    Dim dDate As Date
    dDate = DateAdd("M", 1, sDate)
    LastDay = Day(DateSerial(Year(dDate), Month(dDate), 0))

End Function

'获取 CPU 序列号
Public Function SYSTEM_GET_CPU_SN()
    Dim sResultArray() As String
    Dim lNo As Long
    Dim sSerialNumber As String
    Dim oSWbemObjectSet As Object
    Dim oSWbemObjectEx As Object
    
    Set oSWbemObjectSet = GetObject("Winmgmts:").InstancesOf("Win32_Processor")
    
    For Each oSWbemObjectEx In oSWbemObjectSet
        lNo = lNo + 1
        sSerialNumber = CStr(oSWbemObjectEx.ProcessorId)
        ReDim Preserve sResultArray(1 To lNo)
        sResultArray(lNo) = sSerialNumber
    Next
    SYSTEM_GET_CPU_SN = Application.WorksheetFunction.transpose(sResultArray)
End Function

' 获取硬盘型号
Public Function SYSTEM_GET_HDISK_TYPENAME()
    Dim sHDid As String
    Dim oSWbemObjectSet As Object
    Dim oSWbemObjectEx As Object
    Dim sResultArray() As String
    Dim lNo As Long
    
    Set oSWbemObjectSet = GetObject("Winmgmts:").InstancesOf("Win32_DiskDrive")
    
    For Each oSWbemObjectEx In oSWbemObjectSet
        sHDid = oSWbemObjectEx.Model
        lNo = lNo + 1
        ReDim Preserve sResultArray(1 To lNo)
        sResultArray(lNo) = sHDid
    Next
    SYSTEM_GET_HDISK_TYPENAME = Application.WorksheetFunction.transpose(sResultArray)
End Function

' 获取逻辑磁盘盘符
Public Function SYSTEM_GET_LDISK_DRIVEID()
    Dim sResultArray() As String
    Dim lNo As Long
    
    Dim oSWbemServices As Object
    Dim oSWbemObjectSet As Object
    Dim oDisk As Object
    
    Set oSWbemServices = GetObject("winmgmts:")
    Set oSWbemObjectSet = oSWbemServices.InstancesOf("Win32_LogicalDisk")
    Set oSWbemObjectSet = GetObject("winmgmts:").InstancesOf("Win32_LogicalDisk")
    
    For Each oDisk In GetObject("winmgmts:").InstancesOf("Win32_LogicalDisk")
        lNo = lNo + 1
        ReDim Preserve sResultArray(1 To lNo)
        sResultArray(lNo) = oDisk.DeviceID
    Next
    SYSTEM_GET_LDISK_DRIVEID = Application.WorksheetFunction.transpose(sResultArray)
End Function

Public Function REGEX_MATCH_IPV4(ByVal ipAddress As String)
    With Globals.RegExObject
        .Global = True
        .Pattern = "^(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[0-9]{1,2})(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[0-9]{1,2})){3}$"
        REGEX_MATCH_IPV4 = .Test(ipAddress)
    End With
End Function

' 相交
' ?IntersectString("a,b,c,d,e,f", "c,d,g,i", ",")
' c,d
Function IntersectString(source, target, delimiter)
    Dim i As Long
    Dim sourceArray() As String
    Dim result As String
    
    target = target & delimiter
    sourceArray = Split(source, delimiter)
    For i = LBound(sourceArray) To UBound(sourceArray)
        If InStr(1, target, sourceArray(i) & delimiter, vbTextCompare) > 0 Then
            result = result & delimiter & sourceArray(i)
        End If
    Next i
    
    If result <> "" Then
        result = Mid(result, 2)
    End If
    
    IntersectString = result
End Function

' 排除
' ?UncrossedString("a,b,c,d,e,f", "c,d,g,i", ",")
' a,b,e,f
Function UncrossedString(source, target, delimiter)
    Dim i As Long
    Dim sourceArray() As String
    Dim result As String
    
    target = target & delimiter
    sourceArray = Split(source, delimiter)
    For i = LBound(sourceArray) To UBound(sourceArray)
        If InStr(1, target, sourceArray(i) & delimiter, vbTextCompare) = 0 Then
            result = result & delimiter & sourceArray(i)
        End If
    Next i
    
    If result <> "" Then
        result = Mid(result, 2)
    End If
    
    UncrossedString = result
End Function
