VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Log"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'<component name="Log">
'  <summary>Excel VBA 日志管理类</summary>
'  <remarks>
'    1) 如果配置文件为工作表，则工作表的规范为：
'       A1、A2、A3 分别代表错误日志、信息日志、调试日志，值 1 表示启用日志
'       A4 表示一次记录日志的最大字符长度。
'          空值     代表 200(默认值)；
'          0 值     代表无限制；
'          >0 值    代表具体的最大字符长度。
'          其它值   代表默认值 200
'       A5 为日志文件全路径。空值或不合法路径取默认路径，即工作簿同路径下的【工作簿名.log】
'    2) 配置文件规范：
'       <config>
'           <log error="1|0" info="1|0" debug="1|0" file="" maxLength="" />
'       </config>
'  </remarks>
'</component>
Option Explicit

' 日志配置源
Public Enum ConfigSourceConstants
    lcsWorksheet = 0                ' 工作表
    lcsXmlFile = 1                  ' Xml配置文件
End Enum

'#region 模块级变量
Private blnConfigurationReady As Boolean
Private strWorkbookName As String
'#endregion

'#region 属性变量
Private m_enuConfigSource As ConfigSourceConstants
Private m_blnLogErrorEnabled As Boolean
Private m_blnLogInfoEnabled As Boolean
Private m_blnLogDebugEnabled As Boolean
Private m_strLogFile As String
Private m_strConfigFile As String
Private m_lngMaxLength As Long

#If DEBUG_MODE = 1 Then
    Private m_objWorkbook As Workbook
#Else
    Private m_objWorkbook As Object
#End If
'#endregion 属性变量

Private Sub Class_Initialize()
On Error GoTo Catch_Label

    '默认日志大小 200 字符
    m_lngMaxLength = 200
    
    Call Me.Init(ThisWorkbook, lcsWorksheet)

Finally_Label:
    Exit Sub
    
Catch_Label:
    'Call Err.Push("NGL.Log.Class_Initialize")
    Call Log.WriteError(Err, True)
    GoTo Finally_Label
    
End Sub


'#region 公共成员
'<procedure name="Dispose">
'  <summary>资源回收</summary>
'</procedure>
Public Sub Dispose()
    On Error Resume Next
    Set m_objWorkbook = Nothing
End Sub

'<procedure name="Init">
'  <summary>初识化</summary>
'  <params>
'    <param name="oWorkbook" datatype="Object">工作簿对象</param>
'    <param name="eConfigSource" datatype="ConfigSourceConstants" optional="1">位置文件数据源类型</param>
'  </params>
'  <remarks>
'    如果 eConfigSource = lcsWorksheet，则日志配置信息被当作存储在 oWorkbook 工作簿中的工作表中，
'        且要求工作表名为工作簿名（不带扩展名），但最大长度 31 个字符。
'    如果 eConfigSource = lcsXmlFile，则日志配置信息保存在与工作簿同名的
'  </remarks>
'</procedure>
Public Sub Init( _
    ByRef oWorkbook As Object, _
    Optional ByVal eConfigSource As ConfigSourceConstants, _
    Optional ByVal sConfigFile As String)

On Error GoTo Catch_Label
    
    Set m_objWorkbook = oWorkbook
        
    ' 去掉扩展名
    strWorkbookName = m_GetWorkbookName(m_objWorkbook.Name)
    
    ' 仅支持已经保存过的工作簿 vba 日志
    If m_objWorkbook.Path <> "" Then
        ' 重置日志配置选项
        Call m_ResetConfig
        
        ' 加载配置信息
        If eConfigSource = lcsWorksheet Then
            Dim oSheet As Object
            
            ' Excel 工作表名的长度最大为 31 个字符
            Set oSheet = m_objWorkbook.Worksheets(Left(strWorkbookName, 31))
            
            Call m_LoadConfigSheet(oSheet)
        ElseIf eConfigSource = lcsXmlFile Then
            If m_FileExists(sConfigFile) Then
                m_strConfigFile = sConfigFile
            Else
                m_strConfigFile = m_objWorkbook.Path & "\" & strWorkbookName & ".xml"
            End If
            
            Call m_LoadConfigXml(m_strConfigFile)
        End If
    End If
    
    blnConfigurationReady = True
    
Finally_Label:
    Exit Sub
    
Catch_Label:
    'Err.Push ("VBAProject.Log.Init")
    GoTo Finally_Label
End Sub

Public Sub WriteError(oError As Object, Optional ByVal bTrace As Boolean)

On Error GoTo Catch_Label

    If Me.LogErrorEnabled Then
        Dim sLog As String
        If bTrace Then
            sLog = oError.Description & vbCrLf & oError.StackTrace
        Else
            sLog = oError.Description
        End If
        m_WriteLog ("|ERROR|Exception:" + sLog)
    End If

Finally_Label:
    Exit Sub
    
Catch_Label:
    'Err.Push ("VBAProject.Log.WriteError")
    GoTo Finally_Label
    
End Sub

Public Sub WriteInfo(sMessage As String)

On Error GoTo Catch_Label

    If Me.LogInfoEnabled Then
        m_WriteLog ("|INFO|" + sMessage)
    End If

Finally_Label:
    Exit Sub
    
Catch_Label:
    'Err.Push ("VBAProject.Log.WriteInfo")
    GoTo Finally_Label
End Sub

Public Sub WriteDebug(sMessage As String)

On Error GoTo Catch_Label

    If Me.LogDebugEnabled Then
        m_WriteLog ("|DEBUG|" + sMessage)
    End If

Finally_Label:
    Exit Sub
    
Catch_Label:
    'Err.Push ("VBAProject.Log.WriteDebug")
    GoTo Finally_Label
End Sub

Public Property Get LogFile() As String
    LogFile = m_strLogFile
End Property

Public Property Get LogInfoEnabled() As Boolean
    LogInfoEnabled = m_blnLogInfoEnabled
End Property

Public Property Get LogDebugEnabled() As Boolean
    LogDebugEnabled = m_blnLogDebugEnabled
End Property

Public Property Get LogErrorEnabled() As Boolean
    LogErrorEnabled = m_blnLogErrorEnabled
End Property

Public Property Get ConfigSource() As ConfigSourceConstants
    ConfigSource = m_enuConfigSource
End Property

Public Property Let ConfigSource(ByVal enuConfigSource As ConfigSourceConstants)
    m_enuConfigSource = enuConfigSource
End Property

Public Property Get ConfigurationFile() As String
    ConfigurationFile = m_strConfigFile
End Property

Public Property Get MaxLength() As Long
    MaxLength = m_lngMaxLength
End Property
'#endregion 公共成员




'#region 私有成员
'<procedure name="m_ResetConfig">
'  <summary>重置日志配置选项</summary>
'</procedure>
Private Sub m_ResetConfig()

Dim sWorkbookName As String
On Error GoTo Catch_Label

    m_blnLogErrorEnabled = True
    m_blnLogInfoEnabled = False
    m_blnLogDebugEnabled = False
    m_lngMaxLength = 200
    m_strLogFile = ""
    
    ' 仅支持已经保存过的工作簿 vba 日志
    If m_objWorkbook.Path <> "" Then
        ' 去掉扩展名
        sWorkbookName = m_GetWorkbookName(m_objWorkbook.Name)
        
        ' 默认日志文件全路径
        m_strLogFile = m_objWorkbook.Path & "\" & sWorkbookName & ".log"
    End If
    
Finally_Label:
    Exit Sub
    
Catch_Label:
    'Err.Push ("VBAProject.Log.m_ResetConfig")
    GoTo Finally_Label

End Sub

Private Sub m_LoadConfigSheet( _
    Optional ByRef oSheet As Object)

On Error GoTo Catch_Label
    
    With oSheet
        Dim sError As String
        Dim sInfo As String
        Dim sDebug As String
        Dim sFile As String
        Dim sMaxLength As String
        Dim lMaxLength As Long
        
        sError = .Cells(1, 1).Value
        sInfo = .Cells(2, 1).Value
        sDebug = .Cells(3, 1).Value
        sMaxLength = .Cells(4, 1).Value
        sFile = .Cells(5, 1).Value
        
        If IsNumeric(sError) Then
            m_blnLogErrorEnabled = (CLng(sError) = 1)
        End If
        
        If IsNumeric(sInfo) Then
            m_blnLogInfoEnabled = (CLng(sInfo) = 1)
        End If
        
        If IsNumeric(sDebug) Then
            m_blnLogDebugEnabled = (CLng(sDebug) = 1)
        End If
        
        If Trim(sFile) <> "" Then
            m_strLogFile = Trim(sFile)
        End If
        
        If IsNumeric(sMaxLength) Then
            Call m_TryParseLng(sMaxLength, lMaxLength)
            If lMaxLength >= 0 Then
                m_lngMaxLength = lMaxLength
            End If
        End If
    End With

Finally_Label:
    Exit Sub
    
Catch_Label:
    'Err.Push ("VBAProject.Log.m_LoadConfigSheet")
    GoTo Finally_Label
End Sub

Private Sub m_LoadConfigXml( _
    Optional ByVal sConfigurationFile As String)

#If DEBUG_MODE = 1 Then
    Dim oDoc As MSXML2.DOMDocument
    Dim oNode As MSXML2.IXMLDOMElement
#Else
    Dim oDoc As Object
    Dim oNode As Object
#End If

On Error GoTo Catch_Label
    
    sConfigurationFile = Trim(sConfigurationFile)
    
    If sConfigurationFile <> "" Then
        Set oDoc = CreateObject("MSXML2.DOMDocument")
        If Not oDoc.Load(sConfigurationFile) Then
            Err.Raise oDoc.parseError.ErrorCode _
                , oDoc.parseError.srcText _
                , oDoc.parseError.Line & ":" & oDoc.parseError.reason
        End If
        
        ' 这里使用相对路径可以照顾配置文件兼容性和移植性
        Set oNode = oDoc.SelectSingleNode("//config/log")
        
        If Not oNode Is Nothing Then
            Dim sError As String
            Dim sInfo As String
            Dim sDebug As String
            Dim sFile As String
            Dim sMaxLength As String
            Dim lMaxLength As Long
            
            sError = Trim(oNode.getAttribute("error") & "")
            sInfo = Trim(oNode.getAttribute("info") & "")
            sDebug = Trim(oNode.getAttribute("debug") & "")
            sFile = Trim(oNode.getAttribute("file") & "")
            sMaxLength = Trim(oNode.getAttribute("maxLength") & "")
            
            If IsNumeric(sError) Then
                m_blnLogErrorEnabled = (CLng(sError) = 1)
            End If
            
            If IsNumeric(sInfo) Then
                m_blnLogInfoEnabled = (CLng(sInfo) = 1)
            End If
            
            If IsNumeric(sDebug) Then
                m_blnLogDebugEnabled = (CLng(sDebug) = 1)
            End If
            
            If Trim(sFile) <> "" Then
                m_strLogFile = sFile
            End If
            
            If IsNumeric(sMaxLength) Then
                Call m_TryParseLng(sMaxLength, lMaxLength)
                If lMaxLength >= 0 Then
                    m_lngMaxLength = lMaxLength
                End If
            End If
        End If
    End If

Finally_Label:
    Set oNode = Nothing
    Set oDoc = Nothing
    Exit Sub
    
Catch_Label:
    'Err.Push ("VBAProject.Log.m_LoadConfigSheet")
    GoTo Finally_Label
End Sub

Private Sub m_TryParseLng(ByVal sValue As String, ByRef lParsed As Long)

On Error GoTo Catch_Label
    
    lParsed = 0
    lParsed = CLng(sValue)

Finally_Label:
    Exit Sub
    
Catch_Label:
    GoTo Finally_Label
    
End Sub

'判断某个文件是否存在
'输入参数：文件全路径
Private Function m_FileExists%(filename$)

' Description
'     Checks 'filename$' to find wether the filename given
'     exists.
'
' Parameters
'     Name              Type     Value
'     -------------------------------------------------------------
'     filename$         String   The filename to be checked
'
' Returns
'     True if the file exists
'     False if the file does not exist
'
' Last updated by Jens Balchen 21.11.95


Dim F%

   ' Trap any errors that may occur
   On Error Resume Next

   ' Get a free file handle to avoid using a file handle already in use
   F% = FreeFile
   ' Open the file for reading
   Open filename$ For Input As #F%
   ' Close it
   Close #F%
   ' If there was an error, Err will be <> 0. In that case, we return False
   m_FileExists% = Not (Err <> 0)

End Function

Private Sub m_WriteLog( _
    ByVal sMessage As String)
    
On Error GoTo Catch_Label

    Open m_strLogFile For Append As #1
    
    If Me.MaxLength > 0 Then
        sMessage = Left(sMessage, Me.MaxLength)
    End If
    
    Print #1, Format(Now, "yyyy-MM-dd hh:mm:ss:ms") & ":" & sMessage
    Close #1

Finally_Label:
    Exit Sub
    
Catch_Label:
    'Err.Push ("VBAProject.Log.m_WriteLog")
    GoTo Finally_Label
End Sub

Private Function m_GetWorkbookName(filename As String) As String
    Dim lIndex As Long
    Dim sReturn As String
    
    sReturn = filename
    
    lIndex = InStrRev(filename, ".")
    If lIndex > 0 Then
        sReturn = Left(sReturn, lIndex - 1)
    End If
    
    m_GetWorkbookName = sReturn
End Function
'#region 私有成员
Private Sub Class_Terminate()
    Call Dispose
End Sub


