VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Error"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'<component name="Error">
'  <summary>VBA.ErrObject类的重新封装</summary>
'  <remarks>可记录错误的调用堆栈</remarks>
'</component>
Option Explicit

Private e() As ErrObjectState
Private Type ErrObjectState
    Description As String
    helpContext As Long
    HelpFile    As String
    Number      As Long
    StackTrace  As String
End Type

Private m_strStackTrace As String
Private m_strDescription As String
Private m_lngNumber As Long
Private m_strSource As String
Private m_lnghelpContext As Long
Private m_strHelpFile As String

Public Property Get StackTrace() As String

On Error GoTo Catch_Label

    StackTrace = e(UBound(e)).StackTrace

Finally_Label:
    Exit Property
    
Catch_Label:
    GoTo Finally_Label
    
End Property

Public Property Let StackTrace(ByVal strStackTrace As String)

On Error GoTo Catch_Label
    
    e(UBound(e)).StackTrace = strStackTrace

Finally_Label:
    Exit Property
    
Catch_Label:
    GoTo Finally_Label
    
End Property

Public Property Get Description() As String
    Description = m_strDescription 'VBA.Err.Description
End Property

Public Property Let Description(ByVal vNewValue As String)
    m_strDescription = vNewValue
    'VBA.Err.Description = vNewValue
End Property

Public Property Get helpContext() As Long
    helpContext = m_lnghelpContext
    'helpContext = VBA.Err.helpContext
End Property

Public Property Let helpContext(ByVal vNewValue As Long)
    m_lnghelpContext = vNewValue
    'VBA.Err.helpContext = vNewValue
End Property

Public Property Get HelpFile() As String
    HelpFile = m_strHelpFile
    'HelpFile = VBA.Err.HelpFile
End Property

Public Property Let HelpFile(ByVal vNewValue As String)
    m_strHelpFile = vNewValue
    'VBA.Err.HelpFile = vNewValue
End Property

Public Property Get Number() As Long
    Number = m_lngNumber
    'Number = VBA.Err.Number
End Property

Public Property Let Number(ByVal vNewValue As Long)
    m_lngNumber = vNewValue
    'VBA.Err.Number = vNewValue
End Property

Public Property Get Source() As String
    Source = m_strSource
    'Source = VBA.Err.Source
End Property

Public Property Let Source(ByVal vNewValue As String)
    m_strSource = vNewValue
    'VBA.Err.Source = vNewValue
End Property

'================================================================================
'  公有函数.
'================================================================================

Public Sub Clear()

On Error GoTo Catch_Label

    VBA.Err.Clear
    
    Description = VBA.Err.Description
    helpContext = VBA.Err.helpContext
    HelpFile = VBA.Err.HelpFile
    Number = VBA.Err.Number
    Me.StackTrace = ""

Finally_Label:
    Exit Sub
    
Catch_Label:
    GoTo Finally_Label

End Sub

'错误压栈
Public Sub Push(Optional StackTrace As String)

    ReDim Preserve e(UBound(e) + 1) As ErrObjectState
    
    With Me
        .Description = VBA.Err.Description
        .helpContext = VBA.Err.helpContext
        .HelpFile = VBA.Err.HelpFile
        .Number = VBA.Err.Number
        .Source = VBA.Err.Source
        .StackTrace = StackTrace
    End With
    
    With e(UBound(e))
        .Description = Me.Description
        .helpContext = Me.helpContext
        .HelpFile = Me.HelpFile
        .Number = Me.Number
        .StackTrace = Me.StackTrace
    End With

End Sub

'冒泡错误
Private Sub Pop()

    With e(UBound(e))

        Description = .Description
        helpContext = .helpContext
        HelpFile = .HelpFile
        Number = .Number
        Me.StackTrace = .StackTrace
    End With

    If UBound(e) Then
        ReDim Preserve e(UBound(e) - 1) As ErrObjectState
    Else
        VBA.Err.Raise Number:=28 ' Out of stack space - underflow
    End If

End Sub

Public Sub Raise(Number As Long, Optional Source, Optional Description, Optional HelpFile, Optional helpContext)
    VBA.Err.Raise Number, Source, Description, HelpFile, helpContext
End Sub

Private Sub Class_Initialize()

    ReDim e(0) As ErrObjectState

End Sub

Private Sub Class_Terminate()
    
    ReDim e(0) As ErrObjectState
    'Erase e()

End Sub

Public Property Get Count() As Variant
    Count = UBound(e)
End Property

'获得栈中的所有错误信息，同时清除错误信息。
Public Function ToString( _
    Optional ByVal bIncludeStackTrace As Boolean = False) As String
    
    Dim i As Integer
    Dim sError As String
    For i = Me.Count To 1 Step -1
        If bIncludeStackTrace Then
            sError = sError & e(i).Description & IIf(Trim(e(i).Description) = "", "", vbCrLf) & e(i).StackTrace & vbCrLf
        Else
            sError = sError & e(i).Description & vbCrLf
        End If
        Pop
    Next
    If Len(sError) > 0 Then
        sError = Left(sError, Len(sError) - 2)
    End If
    ToString = sError
    
End Function

Public Sub PopAll()
    Dim i As Integer
    For i = Me.Count To 1 Step -1
        Pop
    Next
End Sub

