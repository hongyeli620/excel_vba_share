VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Filter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'<enum name="DFHOperationTypeConstants">
'  <summary>条件间操作类型枚举</summary>
'</enum>
Public Enum DFHOperationTypeConstants
    dfhOperationAnd                         ' 并且
    dfhOperationOr                          ' 或者
End Enum

'<enum name="DFHOperatorConstants">
'  <summary>操作符枚举</summary>
'</enum>
Public Enum DFHOperatorConstants
    dfhOperatorEquals = 0                   ' =
    dfhOperatorDoesNotEqual = 1             ' <>
    dfhOperatorLessThan = 2                 ' <
    dfhOperatorEqualsAndLessThan = 3        ' <=
    dfhOperatorGreaterThan = 4              ' >
    dfhOperatorEqualsAndGreaterThan = 5     ' >=
    dfhOperatorContants = 6                 ' Like
    dfhOperatorDoesNotContant = 7           ' Not Like
    dfhOperatorBeginWith = 8                ' Like "key*"
    dfhOperatorEndWith = 9                  ' Like "*key"
End Enum

Private m_enuOperationType As DFHOperationTypeConstants
Private m_blnMatchCase As Boolean
Private m_enuOperator As DFHOperatorConstants
Private m_varValue As Variant
Private m_varColumnIndex As Variant
Private m_blnGroupWithPrevious As Boolean

Private Sub Class_Initialize()
    m_blnMatchCase = False
    m_enuOperator = dfhOperatorEquals
    m_enuOperationType = dfhOperationAnd
    m_varColumnIndex = 1
    m_blnGroupWithPrevious = True
End Sub

'<procedure name="OperationType" rw="__/rw">
'  <summary>获取或设置条件间的操作类型</summary>
'  <returns datatype="DFHOperationTypeConstants" />
'</procedure>
Public Property Get OperationType() As DFHOperationTypeConstants
Attribute OperationType.VB_Description = "获取或设置条件间的操作类型"
    OperationType = m_enuOperationType
End Property

Public Property Let OperationType(ByVal enuOperationType As DFHOperationTypeConstants)
    m_enuOperationType = enuOperationType
End Property

'<procedure name="MatchCase" rw="__/rw">
'  <summary>获取或设置文本匹配类型</summary>
'  <returns datatype="Boolean">
'    <return value="True">二进制匹配</return>
'    <return value="False">文本匹配(不屈分大小写)</return>
'  </returns>
'</procedure>
Public Property Get MatchCase() As Boolean
Attribute MatchCase.VB_Description = "获取或设置文本匹配类型"
    MatchCase = m_blnMatchCase
End Property

Public Property Let MatchCase(ByVal blnMatchCase As Boolean)
    m_blnMatchCase = blnMatchCase
End Property

'<procedure name="Operator" rw="__/rw">
'  <summary>获取或设置操作符</summary>
'  <returns datatype="DFHOperatorConstants" />
'</procedure>
Public Property Get Operator() As DFHOperatorConstants
Attribute Operator.VB_Description = "获取或设置操作符"
    Operator = m_enuOperator
End Property

Public Property Let Operator(ByVal enuOperator As DFHOperatorConstants)
    m_enuOperator = enuOperator
End Property

'<procedure name="Value" rw="__/rw">
'  <summary>获取或设置条件值</summary>
'  <returns datatype="Variant" />
'</procedure>
Public Property Get Value() As Variant
Attribute Value.VB_Description = "获取或设置条件值"
    Value = m_varValue
End Property

Public Property Let Value(ByVal varValue As Variant)
    m_varValue = varValue
End Property

'<procedure name="ColumnIndex" rw="__/rw">
'  <summary>获取或设置过滤条件所在列索引</summary>
'  <returns datatype="Variant" />
'  <remarks>如果数据源包含标题,则可以用标题名称来匹配列位置</remarks>
'</procedure>
Public Property Get ColumnIndex() As Variant
Attribute ColumnIndex.VB_Description = "获取或设置过滤条件所在列索引"
    ColumnIndex = m_varColumnIndex
End Property

Public Property Let ColumnIndex(ByVal varColumnIndex As Variant)
    m_varColumnIndex = varColumnIndex
End Property

'<procedure name="Value" rw="__/rw">
'  <summary>获取或设置是否与前一条件为一组条件</summary>
'  <returns datatype="Boolean" />
'</procedure>
Public Property Get GroupWithPrevious() As Boolean
Attribute GroupWithPrevious.VB_Description = "获取或设置是否与前一条件为一组条件"
    GroupWithPrevious = m_blnGroupWithPrevious
End Property

Public Property Let GroupWithPrevious(ByVal blnGroupWithPrevious As Boolean)
    m_blnGroupWithPrevious = blnGroupWithPrevious
End Property
