VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DataArrayHelper"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private m_blnDirty As Boolean
Private m_varDataArray As Variant
Private m_varDataSourceArray As Variant
Private m_varDataHeaderArray As Variant
Private m_blnIncludeHeader As Boolean
Private m_objCompare As ICompare
Private m_objCompareFactory As New CompareFactory

'<procedure name="IncludeHeader" rw="_/r_">
'  <summary>获取数据区域是否包含标题行</summary>
'  <returns datatype="Boolean">
'    <return value="True">包含</return>
'    <return value="False">不包含</return>
'  </returns>
'</procedure>
Public Property Get IncludeHeader() As Boolean
Attribute IncludeHeader.VB_Description = "获取数据区域是否包含标题行"
    IncludeHeader = m_blnIncludeHeader
End Property

'<procedure name="DataArray" rw="__/r_">
'  <summary>获取上一次过滤后的数据的二维数组</summary>
'  <returns datatype="Variant" />
'</procedure>
Public Property Get DataArray() As Variant
Attribute DataArray.VB_Description = "获取上一次过滤后的数据的二维数组"
    DataArray = m_varDataArray
End Property

'<procedure name="DataSourceArray" rw="__/r_">
'  <summary>获取源数据区域的二维数组</summary>
'  <returns datatype="Variant" />
'</procedure>
Public Property Get DataSourceArray() As Variant
Attribute DataSourceArray.VB_Description = "获取源数据区域的二维数组"
    DataSourceArray = m_varDataSourceArray
End Property

'<procedure name="DataSourceArray" rw="__/r_">
'  <summary>获取源数据标题区域的二维数组</summary>
'  <returns datatype="Variant" />
'</procedure>
Public Property Get DataHeaderArray() As Variant
    DataHeaderArray = m_varDataHeaderArray
End Property

'<procedure name="Dirty" rw="__/rw">
'  <summary>获取源数据区域是否被滤过过</summary>
'  <returns datatype="Boolean">
'    <return value="True">是</return>
'    <return value="False">否</return>
'  </returns>
'  <remarks>如果返回 true,表示已经过滤并缓存了数据,可以通过 Data 属性获取上一次过滤的结果</remarks>
'</procedure>
Public Property Get Dirty() As Boolean
Attribute Dirty.VB_Description = "获取源数据区域是否被滤过过"
    Dirty = m_blnDirty
End Property

Public Property Let Dirty(ByVal blnDirty As Boolean)
    m_blnDirty = blnDirty
End Property

'<procedure name="Init">
'  <summary>初始化</summary>
'  <params>
'    <param name="Target" datatype="Excel.Range" out="1">目标区域</param>
'    <param name="IncludeHeader" datatype="Boolean" optional="1" default="True" >True=目标区域包含标题行</param>
'  </params>
'</procedure>
Public Sub Init( _
    Target As Variant, _
    Optional ByVal IncludeHeader As Boolean = True)
    
Dim avHeader As Variant
Dim i As Long
    
Dim lErrNumber As Long
Dim sMessage As String
Dim sSource As String

On Error GoTo Catch_Label

    m_blnIncludeHeader = IncludeHeader
    m_varDataHeaderArray = avHeader
    
    If IsObject(Target) Then
        If TypeOf Target Is Excel.Range Then
            If Target.Count = 1 Then
                ReDim m_varDataSourceArray(1 To 1, 1 To 1)
                m_varDataSourceArray(1, 1) = Target.Value
            Else
                m_varDataSourceArray = Target.Value
            End If
        Else
            VBA.Err.Raise vbObjectError + 100, "", "无效的对象参数类型 Target。"
        End If
    Else
        If IsArray(Target) Then
            m_varDataSourceArray = Target
        Else
            VBA.Err.Raise vbObjectError + 100, "", "无效的参数类型 Target。"
        End If
    End If
    
    If m_blnIncludeHeader Then
        ReDim avHeader(1 To 1, LBound(m_varDataSourceArray, 2) To UBound(m_varDataSourceArray, 2))
        For i = LBound(avHeader, 2) To UBound(avHeader, 2)
            avHeader(1, i) = m_varDataSourceArray(LBound(m_varDataSourceArray), i)
        Next i
        m_varDataHeaderArray = avHeader
    End If

Finally_Label:
    Exit Sub

Catch_Label:
    lErrNumber = VBA.Err.Number
    sSource = "Sub Init of Class Module DataArrayHelper" & " | " & VBA.Err.Source
    sMessage = VBA.Err.Description

    VBA.Err.Raise lErrNumber, sSource, sMessage
End Sub

'<procedure name="FilterSimple">
'  <summary>单列过滤并返回符合条件的数据的二维数组</summary>
'  <returns datatype="Variant" />
'  <params>
'    <param name="ColIndex" datatype="Variant" >列索引。如果 IncludeHeader = True, 则可以用标题名作为列索威；否则只能将数组区域的列标顺序号作为列索引值传入。</param>
'    <param name="Value1" datatype="Variant" >查找的值。</param>
'    <param name="Operator1" datatype="String" optional="1" default="=" >操作符。用于标识条件匹配类型，支持 =, >, <, >=, <=, <>, like, not like</param>
'    <param name="Value2" datatype="Variant" >查找的值。用于区间条件的另一个条件值</param>
'    <param name="Operator2" datatype="String" optional="1" default="=" >操作符。用于标识条件匹配类型，支持 =, >, <, >=, <=, <>, like, not like</param>
'    <param name="MatchCase" datatype="Boolean" optional="1" default="False" >匹配大小写</param>
'  </params>
'</procedure>
Public Function FilterSimple( _
    ByVal ColIndex As Variant, _
    ByVal Value1 As Variant, _
    Optional ByVal Operator1 As String = "=", _
    Optional ByVal Value2 As Variant, _
    Optional ByVal Operator2 As String = "=", _
    Optional ByVal MatchCase As Boolean) As Variant
Attribute FilterSimple.VB_Description = "单列过滤并返回符合条件的数据的二维数组"

Dim lCol As Long
Dim lStartRow As Long
Dim i As Long, j As Long
Dim avRecords As Variant
Dim n As Long

Dim bMatch As Boolean

Dim lErrNumber As Long
Dim sMessage As String
Dim sSource As String

On Error GoTo Catch_Label
    
    ' 重新初始化用于存储过滤数据的缓存
    m_varDataArray = avRecords
    Me.Dirty = True
    
    If IsNumeric(ColIndex) Then
        lCol = ColIndex
    Else
        If Me.IncludeHeader Then
            '如果传入的是列标题名称，需要计算列位置
            lCol = GetColIndex(ColIndex)
        End If
    End If
    
    ' 源数据无数据行
    If (UBound(m_varDataSourceArray) = 1 And Me.IncludeHeader) _
        Or (UBound(m_varDataSourceArray) = 0 And Not Me.IncludeHeader) Then
        
        GoTo Finally_Label
    End If
    
    ' 起始行
    lStartRow = -Me.IncludeHeader + 1
    
    ReDim avRecords(LBound(m_varDataSourceArray, 1) To UBound(m_varDataSourceArray, 1), LBound(m_varDataSourceArray, 2) To UBound(m_varDataSourceArray, 2))
    
    If MatchCase Then
        Set m_objCompare = m_objCompareFactory.GetInstance(vbBinaryCompare)
    Else
        Set m_objCompare = m_objCompareFactory.GetInstance(vbTextCompare)
    End If
    
    For i = lStartRow To UBound(m_varDataSourceArray)
        If IsNull(m_varDataSourceArray(i, lCol)) Then
            m_varDataSourceArray(i, lCol) = ""
        End If
        
        Select Case LCase(Operator1)
            Case "="
                If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value1) And TypeName(Value1) <> "String" Then
                    m_varDataSourceArray(i, lCol) = 0
                End If
                bMatch = m_objCompare.Equals(Value1, m_varDataSourceArray(i, lCol))
            Case ">"
                If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value1) And TypeName(Value1) <> "String" Then
                    m_varDataSourceArray(i, lCol) = 0
                End If
                bMatch = (m_varDataSourceArray(i, lCol) > Value1)
                
            Case "<"
                If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value1) And TypeName(Value1) <> "String" Then
                    m_varDataSourceArray(i, lCol) = 0
                End If
                bMatch = (m_varDataSourceArray(i, lCol) < Value1)
                
            Case ">="
                If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value1) And TypeName(Value1) <> "String" Then
                    m_varDataSourceArray(i, lCol) = 0
                End If
                bMatch = (m_varDataSourceArray(i, lCol) >= Value1)
                
            Case "<="
                If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value1) And TypeName(Value1) <> "String" Then
                    m_varDataSourceArray(i, lCol) = 0
                End If
                bMatch = (m_varDataSourceArray(i, lCol) <= Value1)
                
            Case "<>"
                If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value1) And TypeName(Value1) <> "String" Then
                    m_varDataSourceArray(i, lCol) = 0
                End If
                bMatch = m_objCompare.DoesNotEqual(Value1, m_varDataSourceArray(i, lCol))
                
            Case "like"
                bMatch = m_objCompare.Contants(m_varDataSourceArray(i, lCol), Value1)
                
            Case "not like"
                bMatch = m_objCompare.DoesNotContant(m_varDataSourceArray(i, lCol), Value1)
                
        End Select
        
        If Not IsEmpty(Value2) And Not IsMissing(Value2) Then
            Select Case LCase(Operator2)
                Case "="
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value2) And TypeName(Value2) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    bMatch = bMatch And (m_objCompare.Equals(Value2, m_varDataSourceArray(i, lCol)))
                Case ">"
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value2) And TypeName(Value2) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    bMatch = bMatch And (m_varDataSourceArray(i, lCol) > Value2)
                    
                Case "<"
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value2) And TypeName(Value2) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    bMatch = bMatch And (m_varDataSourceArray(i, lCol) < Value2)
                    
                Case ">="
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value2) And TypeName(Value2) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    bMatch = bMatch And (m_varDataSourceArray(i, lCol) >= Value2)
                    
                Case "<="
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value2) And TypeName(Value2) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    bMatch = bMatch And (m_varDataSourceArray(i, lCol) <= Value2)
                    
                Case "<>"
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(Value2) And TypeName(Value2) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    bMatch = bMatch And (m_objCompare.DoesNotEqual(Value2, m_varDataSourceArray(i, lCol)))
                    
                Case "like"
                    bMatch = bMatch And (m_objCompare.Contants(m_varDataSourceArray(i, lCol), Value2))
                    
                Case "not like"
                    bMatch = bMatch And (m_objCompare.DoesNotContant(m_varDataSourceArray(i, lCol), Value2))
                    
            End Select
        End If
        
        If bMatch Then
            n = n + 1
            
            For j = LBound(m_varDataSourceArray, 2) To UBound(m_varDataSourceArray, 2)
                avRecords(n, j) = m_varDataSourceArray(i, j)
            Next j
        End If
    Next i
    
    If n > 0 Then
        ReDim m_varDataArray(LBound(avRecords, 1) To n, LBound(avRecords, 2) To UBound(avRecords, 2))
        
        For i = LBound(m_varDataArray, 1) To n
            For j = LBound(m_varDataArray, 2) To UBound(m_varDataArray, 2)
                m_varDataArray(i, j) = avRecords(i, j)
            Next j
        Next i
    End If
    
    FilterSimple = m_varDataArray
    
Finally_Label:
    Exit Function

Catch_Label:
    lErrNumber = Err.Number
    sSource = "Function FilterSimple of Class Module DataArrayHelper" & " | " & Err.Source
    sMessage = Err.Description

    Err.Raise lErrNumber, sSource, sMessage
End Function

'<procedure name="Filter">
'  <summary>按给定条件过滤源数据并返回二维数组</summary>
'  <returns datatype="Variant" />
'  <params>
'    <param name="Filters" datatype="Filters" out="1">过滤条件集合对象</param>
'  </params>
'</procedure>
Public Function Filter( _
    Filters As Filters _
    , Optional ByRef SourceRowArray As Variant) As Variant

Dim lCol As Long
Dim lStartRow As Long
Dim i As Long, j As Long
Dim avRecords As Variant
Dim n As Long

Dim bMatch As Boolean
Dim alRowArray() As Long

Dim oFilter As Filter
Dim vValue As Variant
Dim lCounter As Long

Dim lErrNumber As Long
Dim sMessage As String
Dim sSource As String

On Error GoTo Catch_Label
    
    ' 重新初始化用于存储过滤数据的缓存
    m_varDataArray = avRecords
    Me.Dirty = True
    
    ' 源数据无数据行
    If (UBound(m_varDataSourceArray) = 1 And Me.IncludeHeader) _
        Or (UBound(m_varDataSourceArray) = 0 And Not Me.IncludeHeader) Then
        
        GoTo Finally_Label
    End If
    
    ' 起始行
    lStartRow = -Me.IncludeHeader + 1
    
    ReDim avRecords(LBound(m_varDataSourceArray, 1) To UBound(m_varDataSourceArray, 1) _
        , LBound(m_varDataSourceArray, 2) To UBound(m_varDataSourceArray, 2))
    
    For i = lStartRow To UBound(m_varDataSourceArray)
        j = 0
        'bMatch = True
        
        For Each oFilter In Filters
            If oFilter.MatchCase Then
                Set m_objCompare = m_objCompareFactory.GetInstance(vbBinaryCompare)
            Else
                Set m_objCompare = m_objCompareFactory.GetInstance(vbTextCompare)
            End If
            
            If IsNumeric(oFilter.ColumnIndex) Then
                lCol = oFilter.ColumnIndex
            Else
                If Me.IncludeHeader Then
                    '如果传入的是列标题名称，需要计算列位置
                    lCol = GetColIndex(oFilter.ColumnIndex)
                End If
            End If
            
            If IsNull(m_varDataSourceArray(i, lCol)) Then
                m_varDataSourceArray(i, lCol) = ""
            End If
            
            vValue = oFilter.Value
            
            Select Case oFilter.Operator
                Case DFHOperatorConstants.dfhOperatorEquals
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(oFilter.Value) And TypeName(oFilter.Value) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    
                    If lCounter = 0 Then
                        bMatch = (m_objCompare.Equals(vValue, m_varDataSourceArray(i, lCol)))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (m_objCompare.Equals(vValue, m_varDataSourceArray(i, lCol)))
                        Else
                            bMatch = bMatch Or (m_objCompare.Equals(vValue, m_varDataSourceArray(i, lCol)))
                        End If
                    End If
                Case DFHOperatorConstants.dfhOperatorDoesNotEqual
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(oFilter.Value) And TypeName(oFilter.Value) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    
                    If lCounter = 0 Then
                        bMatch = (m_objCompare.DoesNotEqual(vValue, m_varDataSourceArray(i, lCol)))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (m_objCompare.DoesNotEqual(vValue, m_varDataSourceArray(i, lCol)))
                        Else
                            bMatch = bMatch Or (m_objCompare.DoesNotEqual(vValue, m_varDataSourceArray(i, lCol)))
                        End If
                    End If
                Case DFHOperatorConstants.dfhOperatorGreaterThan
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(oFilter.Value) And TypeName(oFilter.Value) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    
                    If lCounter = 0 Then
                        bMatch = (vValue > m_varDataSourceArray(i, lCol))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (vValue > m_varDataSourceArray(i, lCol))
                        Else
                            bMatch = bMatch Or (vValue > m_varDataSourceArray(i, lCol))
                        End If
                    End If
                Case DFHOperatorConstants.dfhOperatorEqualsAndGreaterThan
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(oFilter.Value) And TypeName(oFilter.Value) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    
                    If lCounter = 0 Then
                        bMatch = (vValue >= m_varDataSourceArray(i, lCol))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (vValue >= m_varDataSourceArray(i, lCol))
                        Else
                            bMatch = bMatch Or (vValue >= m_varDataSourceArray(i, lCol))
                        End If
                    End If
                Case DFHOperatorConstants.dfhOperatorLessThan
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(oFilter.Value) And TypeName(oFilter.Value) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    
                    If lCounter = 0 Then
                        bMatch = (vValue < m_varDataSourceArray(i, lCol))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (vValue < m_varDataSourceArray(i, lCol))
                        Else
                            bMatch = bMatch Or (vValue < m_varDataSourceArray(i, lCol))
                        End If
                    End If
                Case DFHOperatorConstants.dfhOperatorEqualsAndLessThan
                    If m_varDataSourceArray(i, lCol) = "" And IsNumeric(oFilter.Value) And TypeName(oFilter.Value) <> "String" Then
                        m_varDataSourceArray(i, lCol) = 0
                    End If
                    
                    If lCounter = 0 Then
                        bMatch = (vValue <= m_varDataSourceArray(i, lCol))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (vValue <= m_varDataSourceArray(i, lCol))
                        Else
                            bMatch = bMatch Or (vValue <= m_varDataSourceArray(i, lCol))
                        End If
                    End If
                Case DFHOperatorConstants.dfhOperatorContants
                    If lCounter = 0 Then
                        bMatch = (m_objCompare.Contants(m_varDataSourceArray(i, lCol), vValue))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (m_objCompare.Contants(m_varDataSourceArray(i, lCol), vValue))
                        Else
                            bMatch = bMatch Or (m_objCompare.Contants(m_varDataSourceArray(i, lCol), vValue))
                        End If
                    End If
                Case DFHOperatorConstants.dfhOperatorDoesNotContant
                    If lCounter = 0 Then
                        bMatch = (m_objCompare.DoesNotContant(m_varDataSourceArray(i, lCol), vValue))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (m_objCompare.DoesNotContant(m_varDataSourceArray(i, lCol), vValue))
                        Else
                            bMatch = bMatch Or (m_objCompare.DoesNotContant(m_varDataSourceArray(i, lCol), vValue))
                        End If
                    End If
                Case DFHOperatorConstants.dfhOperatorBeginWith
                    If lCounter = 0 Then
                        bMatch = (m_objCompare.BeginWith(m_varDataSourceArray(i, lCol), vValue))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (m_objCompare.BeginWith(m_varDataSourceArray(i, lCol), vValue))
                        Else
                            bMatch = bMatch Or (m_objCompare.BeginWith(m_varDataSourceArray(i, lCol), vValue))
                        End If
                    End If
                Case DFHOperatorConstants.dfhOperatorEndWith
                    If lCounter = 0 Then
                        bMatch = (m_objCompare.EndWith(m_varDataSourceArray(i, lCol), vValue))
                    Else
                        If oFilter.OperationType = dfhOperationAnd Then
                            bMatch = bMatch And (m_objCompare.EndWith(m_varDataSourceArray(i, lCol), vValue))
                        Else
                            bMatch = bMatch Or (m_objCompare.EndWith(m_varDataSourceArray(i, lCol), vValue))
                        End If
                    End If
            End Select
            
            lCounter = lCounter + 1
        Next oFilter
        
        lCounter = 0
        If bMatch Then
            ReDim Preserve alRowArray(n)
            alRowArray(n) = i
            
            n = n + 1
            
            For j = LBound(m_varDataSourceArray, 2) To UBound(m_varDataSourceArray, 2)
                avRecords(n, j) = m_varDataSourceArray(i, j)
            Next j
        End If
    Next i
    
    If n > 0 Then
        ReDim m_varDataArray(LBound(avRecords, 1) To n, LBound(avRecords, 2) To UBound(avRecords, 2))
        
        For i = LBound(m_varDataArray, 1) To n
            For j = LBound(m_varDataArray, 2) To UBound(m_varDataArray, 2)
                m_varDataArray(i, j) = avRecords(i, j)
            Next j
        Next i
    End If
    
    SourceRowArray = alRowArray
    Filter = m_varDataArray
    
Finally_Label:
    Exit Function

Catch_Label:
    lErrNumber = VBA.Err.Number
    sSource = "Function Filter of Class Module DataArrayHelper" & " | " & VBA.Err.Source
    sMessage = VBA.Err.Description

    VBA.Err.Raise lErrNumber, sSource, sMessage
End Function

Private Function GetColIndex( _
    ByVal Fieldname As String) As Long

Dim lCol As Long
Dim lLoop As Long

    For lLoop = LBound(m_varDataSourceArray, 2) To UBound(m_varDataSourceArray, 2)
        If StrComp(m_varDataSourceArray(1, lLoop), Fieldname, vbTextCompare) = 0 Then
            lCol = lLoop
            Exit For
        End If
    Next lLoop
    
    GetColIndex = lCol
End Function

Public Sub Dispose()
    On Error Resume Next
    Set m_objCompare = Nothing
    If Not m_objCompareFactory Is Nothing Then
        m_objCompareFactory.Dispose
    End If
    Set m_objCompareFactory = Nothing
End Sub
