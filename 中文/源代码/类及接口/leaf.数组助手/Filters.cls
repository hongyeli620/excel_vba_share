VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Filters"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "过滤条件集合"
Option Explicit

Private m_objCollection As Collection

Public Function Add() As Filter
    
Dim oItem As Filter
Dim sKey As String
    
Dim lErrNumber As Long
Dim sMessage As String
Dim sSource As String

On Error GoTo Catch_Label

    Set oItem = New Filter
    
    sKey = "F" & m_objCollection.Count + 1
    m_objCollection.Add oItem, sKey

    Set Add = oItem

Finally_Label:
    Set oItem = Nothing
    Exit Function

Catch_Label:
    lErrNumber = Err.Number
    sSource = "Function Add of Class Module Filters" & " | " & Err.Source
    sMessage = Err.Description

    Set oItem = Nothing
    Err.Raise lErrNumber, sSource, sMessage

End Function

Public Property Get Item(Index As Variant) As Filter
Attribute Item.VB_UserMemId = 0
    Set Item = m_objCollection(Index)
End Property

Public Property Get Count() As Long
    'used when retrieving the number of elements in the
    'collection. Syntax: Debug.Print x.Count
    Count = m_objCollection.Count
End Property

Public Sub Remove(Index As Variant)
    m_objCollection.Remove Index
End Sub

Public Sub Clear()
Dim lLoop As Long

    If m_objCollection.Count = 0 Then
        Exit Sub
    End If
    
    For lLoop = m_objCollection.Count To 1
        m_objCollection.Remove lLoop
    Next lLoop
End Sub

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    Set NewEnum = m_objCollection.[_NewEnum]
End Property


Private Sub Class_Initialize()
    Set m_objCollection = New Collection
End Sub

Private Sub Class_Terminate()
    Set m_objCollection = Nothing
End Sub
