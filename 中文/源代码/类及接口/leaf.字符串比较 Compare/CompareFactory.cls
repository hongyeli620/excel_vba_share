VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CompareFactory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


Private m_objCompareCaseInsensitive  As New CompareCaseInsensitive
Private m_objCompareCaseSensitive  As New CompareCaseSensitive

Private Sub Class_Terminate()
    Call Dispose
End Sub

Public Sub Dispose()
    On Error Resume Next
    Set m_objCompareCaseInsensitive = Nothing
    Set m_objCompareCaseSensitive = Nothing
End Sub

Public Function GetInstance( _
    Optional ByVal CompareMethod As VbCompareMethod) As ICompare
    
    If CompareMethod = vbBinaryCompare Then
        Set GetInstance = m_objCompareCaseSensitive
    ElseIf CompareMethod = vbTextCompare Then
        Set GetInstance = m_objCompareCaseInsensitive
    End If
End Function
