<h1>中文资源</h1>

<h3>VBA</h3>
xls/xlsm 带 vba 的 excel 文件

<h3>加载宏</h3>
xla/xlam 加载宏、COM 加载宏

<h3>讲义</h3>
一些公开课的讲义

<h3>文档</h3>
链路式函数(excel版)

<h3>源代码</h3>
VBE 导出的可重用的源代码文件
VB6 源代码
VSTO 源代码

<h3>在线链接</h3>
有价值的在线资源

