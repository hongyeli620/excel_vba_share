<h1>加载宏</h1>

<h3>leaf.sqlhelper.beta.1.0</h3>
Excel Sql 客户端 ，Excel 文件作为数据源通过 SQL 进行数据查询并将结果返回单元格区域。

<h3>leaf.vba.automation</h3>
VBA 自动化，提供几个宏:<br />
1. 按维度拆分 DimensionToFact<br />
2. 按数量拆行 SplitRow<br />
3. 按分类拆行 SplitByCategory

<h3>leaf.wsf</h3>
一些自定义工作表函数