<h2>Excel VBA 开源项目组</h2>
leaf 创建于 2015-1-12

<h3>这里您可以：</h3>
:heavy_check_mark:下载任何对您有帮助的相关 vba 资源；<br />
:heavy_check_mark:您也可以成为志愿者之一，成为资源贡献者！

<h3>作为一名贡献者：</h3>
:heavy_check_mark:您需要申请一个 git.oschina.net 的账号，并申请加入 <a target="_blank" href="/hongyeli620/excel_vba_temp">VBA资源待审项目组</a> <br />
:heavy_check_mark:上传您的资源，经审核后，资源会正式进入 <a target="_blank" href="/hongyeli620/excel_vba_share">开源项目组</a><br/> 

<h3>上传资源时请您：</h3>
提供用途、使用方法介绍<br />

<h3>贡献者 QQ 群：</h3>
121053731